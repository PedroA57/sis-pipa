#mysql modules
'''
import mysql.connector
from mysql.connector import *
'''
#Tre4gh-L0l

#android modules
#from jnius import autoclass

#kivy modules
import kivy
kivy.require('1.1.1')

#json modules
import json

#to manipulate the data in unicode from json
import unicodedata


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty
from kivy.vector import Vector
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window, WindowBase
from kivy.config import Config
from kivy.animation import Animation, AnimationTransition
from kivy.graphics import *



########################################################################################################################################
######################## CAMADA DE APRESENTACAO ########################################################################################
########################################################################################################################################
#Initial, Initialoged, Configtab -> SManager -> Interface -> App



#Screens Classes
class Initial(Screen):
    def __init__(self, *args, **kwargs):
    	#gets the screen class stuff
        super(Initial, self).__init__(*args, **kwargs)
    
        #adds things to the interface
        #layout of the screen
        self.layout = FloatLayout()
        #background
        self.bck = Button(text='', size_hint=(1, 1), pos_hint={'x': 0, 'y':0}, disabled=True, background_disabled_normal='', background_color= (1, 1, 1, 1))
        #logo
        self.logo = Label(text='<LOGO>', pos_hint={'x': 0, 'y': .2}, color=(0, 0, 0, 1), font_size ='20sp')
        #1: pc, 2: mobile
        self.image='/home/void/Desktop/Scripts/WTA/Images/ti.png'
        #self.image='ti.png'
        #user name input
        self.usernameinput = TextInput(hint_text='Usuario', text='', multiline=False, size_hint=(.8, .05), pos_hint={'x': .1, 'y': .45}, border=(4, 4, 4, 4), background_normal = self.image, background_active = self.image)
        #password name input
        self.passwordinput = TextInput(hint_text='Senha', text='', multiline=False, size_hint=(.8, .05), pos_hint={'x': .1, 'y': .355}, password=True, border=(4, 4, 4, 4), background_normal = self.image, background_active = self.image)
        #login button
        self.lgin = Button(text='Login', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .22}, background_normal='', background_color= (.121, .458, 1, 1))
        #sgn up button
        self.signup = Button(text='Cadastre-se', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .11}, background_normal='', background_color= (.121, .458, 1, 1))
        
        #links this screen to the controller layer
        self.lgin.bind(on_release=control.login)
        self.usernameinput.bind(on_text_validate=control.login)
        self.passwordinput.bind(on_text_validate=control.login)
        self.usernameinput.bind(on_focus=control.loginerror)
        self.passwordinput.bind(on_focus=control.loginerror)
        

        #adds to the layout
        self.layout.add_widget(self.bck)
        self.layout.add_widget(self.passwordinput)
        self.layout.add_widget(self.usernameinput)
        self.layout.add_widget(self.logo)
        self.layout.add_widget(self.lgin)
        self.layout.add_widget(self.signup)
        self.add_widget(self.layout)

    def reset(self):
    	self.usernameinput.hint_text_color=(.5, .5, .5, 1)
    	self.usernameinput.hint_text=''
    	self.usernameinput.text=''



class Initialogged_user(Screen):
    def __init__(self, *args, **kwargs):
        #gets the screen class stuff
        super(Initialogged_user, self).__init__(*args, **kwargs)
    
        #adds things to the interface
        #layout of the screen
        self.layout = FloatLayout()
        #background
        self.bck = Button(text='', size_hint=(1, 1), pos_hint={'x': 0, 'y':0}, disabled=True, background_disabled_normal='', background_color= (1, 1, 1, 1)) 
        #logo
        self.logo = Label(text='<LOGO>', pos_hint={'x': 0, 'y': .25}, color=(0, 0, 0, 1), font_size ='20sp')
        #get water button / pos_hint={'x': .25, 'y': .375(x 1.1)=.4125}
        self.getwater = Button(text='Pedir Agua', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .38}, background_normal='', background_color= (.121, .458, 1, 1))
        #logout button / pos_hint={'x': .25, 'y': .2(x 1.1)=.22}
        self.logout = Button(text='Sair', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .22}, background_normal='', background_color= (.121, .458, 1, 1))
        
        
        #binds the callbacks
        #self.getwater.bind(on_release=self.fgetwater)
        self.logout.bind(on_release=control.logout)
        
        
        #adds to the layout
        self.layout.add_widget(self.bck)
        self.layout.add_widget(self.logo)
        self.layout.add_widget(self.getwater)
        self.layout.add_widget(self.logout)
        self.add_widget(self.layout)



class ConfigTab(Widget):
    def __init__(self, *args, **kwargs):
        #gets the screen class stuff
        super(ConfigTab, self).__init__(*args, **kwargs)
    
        #adds things to the interface
        #layout of the screen (y: .9727)
        x = Window.size[0]*(-0.8)
        self.layout=FloatLayout(size_hint=(.8, 1), pos_hint={'x':-0.8, 'y':0})
        #self.layout.canvas.add(Color(1, 0, 0, 1))
        #self.layout.canvas.add(Rectangle(pos=self.layout.pos, size=self.layout.size))
        #xy of the window
        x = Window.size[0]
        y = Window.size[1]
        #user interface
        self.user_bar=Widget(size_hint=(1, .225), pos_hint={'x': 0, 'y': .775})
        #adds the canvas instructions
        with self.user_bar.canvas:
        	#lightsky color
        	Color(.529, .807, .98, 1)
        	self.rect = Rectangle(pos=self.user_bar.pos, size= self.user_bar.size)
        	Color(1, 1, 1, 1)
        	self.ell = Ellipse(source='/home/void/Desktop/Scripts/WTA/Images/profile-42914_640.png', pos=self.user_bar.pos, size= self.user_bar.size)
        #updates the canvas instructions pos and size
        self.user_bar.bind(pos=control.canvas_update, size=control.canvas_update)
        #accordion
        self.ac = Accordion(orientation='vertical',min_space=y/10, size_hint_y= .775, pos_hint={'x':0, 'y': 0})
        #accordion items
        self.userdata=AccordionItem(title='Dados Pessoais', min_space=(y*10/100), background_selected='atlas://data/images/defaulttheme/button')
        self.fornecedores = AccordionItem(title='Fornecedores', min_space=(y*10/100), background_selected='atlas://data/images/defaulttheme/button')
        self.dadosbancarios = AccordionItem(title='Dados Bancarios', min_space=(y*10/100), background_selected='atlas://data/images/defaulttheme/button')
        

        #adds the contents to each accordion item
        self.ud = ScrollView(do_scroll_x=False, size=(x, y*475/1000), size_hint=(1, None))
        self.gl = GridLayout(cols=1, spacing=y/14, size_hint_y=None)
        self.gl.bind(minimum_height=self.gl.setter('height'))
        #calls the controller function to return the data input length
        arr = []
        length = control.get_data_length(data.json_data["data"], arr)
        print length
        count=0
        for i in range(length+1):
            '''if i == 0:
            	b = Label(text=(''), size_hint=(1, None), height=y/20)
                self.gl.add_widget(b)
                continue
            '''
            if i == 0:
                b = Label(text=('Digite a nova informacao\nna entrada de texto\ncorrespondente para altera-la.'), text_size=(x*9/10, None), halign='center', size_hint=(1, None), height=y/10)
                #b = Button(text='hello world', size_hint=(1, None), height=y/10)
                self.gl.add_widget(b)
                continue            	

            txt=arr[count]
            b = TextInput(text='', hint_text=txt, size_hint=(1, None), height=y/20)
            count+=1
            self.gl.add_widget(b)

        #adds the layout with text input to the scrollview
        self.ud.add_widget(self.gl)
        #adds the contents to each accordionitem
        self.userdata.add_widget(self.ud)
        self.fornecedores.add_widget(Label(text='Hello World'))
        self.dadosbancarios.add_widget(Label(text='Hello World'))
        
        #adds each accordion item to the accordion itself
        self.ac.add_widget(self.userdata)
        self.ac.add_widget(self.fornecedores)
        self.ac.add_widget(self.dadosbancarios)
        #adds the accordion to the layout
        self.layout.add_widget(self.user_bar)
        self.layout.add_widget(self.ac)
        #adds the layout to te screen
        #self.add_widget(self.layout)

class Initialogged_forn(Screen):
	def __init__(self, *args, **kwargs):
		super(Initialogged_forn, self).__init__(*args, **kwargs)

		#the main layout
		self.layout = FloatLayout()

		#

		#adds them to the layout

		#adds the layout to te screen
		self.add_widget(self.layout)



class SManager(ScreenManager):
    def __init__(self, *args, **kwargs):
        #gets the screen class stuff
        super(SManager, self).__init__(*args, **kwargs)

        #enerates the screen objects
        self.initial = Initial(name='initial')
        self.initialogged_user = Initialogged_user(name='initialogged_user')
        self.initialogged_forn = Initialogged_forn(name='initialogged_forn')
        #self.configtab = ConfigTab(name='configtab')
        
    
        #sets its own variables
        self.size_hint=(1, .95)
        self.pos_hint={'x':0, 'y': 0}

        #adds the screen
        self.add_widget(self.initial)
        self.add_widget(self.initialogged_user)
        self.add_widget(self.initialogged_forn)
        #self.add_widget(self.configtab)
        #sets the current screen
        self.current='initial'



interface=0
class Interface(Widget):
    def __init__(self, *args, **kwargs):
        super(Interface, self).__init__(*args, **kwargs)
        #variables
        self.actual=''
        self.cbar = False
        
        self.layout=FloatLayout()
        #screens
        self.SM = SManager()
        #Config tab
        self.config = ConfigTab()
        #top bar
        self.bar = Button(text='', disabled = True, size_hint=(1, .1), pos_hint={'x':0, 'y': .95}, background_disabled_normal='', background_color= (.5, .5, .5, 1))
        #config Button / pos_hint={'x': .01, 'y': .98}
        sizey = Window.size[1]/20
        sizex = sizey*12/10
        #1: pc, 2: mobile
        self.image='/home/void/Desktop/Scripts/WTA/Images/bars_config.png'
        #self.image='bars_config.png'
        self.configbtn = Button(text='', size_hint=(None, None), size=(sizex, sizey), pos_hint={'x': 0, 'y': .95}, background_normal = self.image, background_down = self.image, border = (0, 0, 0, 0))

        #binds the callbacks
        self.configbtn.bind(on_release=control.config)
        
        #adds them to the layout
        self.layout.add_widget(self.SM)
        self.layout.add_widget(self.config.layout)
        self.layout.add_widget(self.bar)
        self.layout.add_widget(self.configbtn)


##########################################################################################################################################
######################### Camada de Dados ################################################################################################
##########################################################################################################################################

#apagar camada de dados, colcoar funcoes na camada de controle
data=0
class Model():
	def __init__(self):
		#/home/void/Desktop/Scripts/WTA/data/json_data
		with open("json_data") as json_file:
			#json_file=json_dt.read()
			self.json_data = json.load(json_file)
			self.logged = False
			
		'''
with open("test.json") as json_file:
    json_data = json.load(json_file)
    print(json_data)
    import json
weather = urllib2.urlopen('url')
wjson = weather.read()
wjdata = json.loads(wjson)
print wjdata['data']['current_condition'][0]['temp_C']
'''
		#connect to te server...
		self.connection = False

	def login(self):
		#for now...
		#validar entrada pelo json
		if interface.SM.initial.usernameinput.text == 'Consumidor':
			return 'Consumidor'
		elif interface.SM.initial.usernameinput.text == 'Fornecedor':
			return 'Fornecedor'

		#print self.json_data["data"]["tipo"]["Fornecedor"]["cartao"]
		#print len(self.json_data["data"]["tipo"]["Fornecedor"])

		#try the login
		#...
		#if its succeds
		#return the type of the login
		#if it doenst
		return False


########################################################################################################################################
######################### Camada de Controle ###########################################################################################
########################################################################################################################################


control=0
class Controller():
	def __init__(self):
		self.bar = False
		self.actual = 0

    #does the login transition
	def login(self, instance):
		if data.login() == 'Consumidor':
			data.logged=True
			interface.SM.transition.direction='left'
			interface.SM.current='initialogged_user'
			interface.SM.initial.reset()
			
		elif data.login() == 'Fornecedor':
			data.logged=True
			interface.SM.transition.direction='left'
			interface.SM.current='initialogged_forn'
			interface.SM.initial.usernameinput.hint_text=''
			interface.SM.initial.usernameinput.hint_text_color=(.5, .5, .5, 1)
			interface.SM.initial.usernameinput.text=''

		else:
			interface.SM.initial.usernameinput.hint_text_color=(1, 0, 0, 1)
			interface.SM.initial.usernameinput.text=''
			interface.SM.initial.usernameinput.hint_text='Usuario ou Senha Incorretas'
			interface.SM.initial.passwordinput.text=''
			interface.SM.initial.passwordinput.hint_text=''

    #informs the user the login failed
	def loginerror(self, instance):
		if interface.SM.initial.usernameinput.hint_text=='Usuario ou Senha Incorretas':
			interface.SM.initial.usernameinput.hint_text=''
			interface.SM.initial.usernameinput.hint_text_color=(.5, .5, .5, 1)
		elif interface.SM.initial.usernameinput.hint_text=='':
			interface.SM.initial.usernameinput.hint_text='Usuario'
			interface.SM.initial.usernameinput.hint_text_color=(.5, .5, .5, 1)
		if interface.SM.initial.passwordinput.hint_text=='':
			interface.SM.initial.passwordinput.hint_text='Senha'
			interface.SM.initial.passwordinput.hint_text_color=(.5, .5, .5, 1)


    #does the logout transition
	def logout(self, instance):
		data.logged=False
		interface.SM.initial.usernameinput.hint_text='Usuario'
		interface.SM.initial.usernameinput.hint_text_color=(.5, .5, .5, 1)
		interface.SM.initial.usernameinput.text=''
		interface.SM.transition.direction='right'
		interface.SM.current='initial'

    #opens and closes the config tab
	def config(self, instance):
		#checks if the user is logged already
		if not data.logged:
			return

		#if the tab is closed, it open it
		if not self.bar:
			x = Window.size[0]*8/10
			opn = Animation(x=x, y=0, t='out_cubic', duration=0.85)
			opn.start(interface.layout)
			self.bar = True

		#if the tab is opened, close it
		elif self.bar:
			x = Window.size[0]*(-8/10)
			opn = Animation(x=0, y=0, t='out_cubic', duration=0.85)
			opn.start(interface.layout)
			#sm.current=self.actual
			self.bar = False

    #returns the data length and adds them to the dict
	def get_data_length(self, data, a):
		#returns the input length of the data parameter received
		length = 0
		for i in data:
			i.encode('ascii','ignore')
			#unicodedata.normalize('NFKD', title).encode('ascii','ignore')
			b = str(type(data[i])) == "<type 'dict'>"
			#b = true if it is a dict, and false if it isnt

			#checks if the data is a dict and this was called in length mode
			if not b:
				a.append(i)
				length+=1
			#if the data is a dict and it was called in length mode:
			else:
				#it calls itself to get length of the dict in the data
				length = length + self.get_data_length(data[i], a)

		return length

	#updates the canvas pos and size
	def canvas_update(self, *args):
		#updates background pos and size
		interface.config.rect.pos=interface.config.user_bar.pos
		interface.config.rect.size=interface.config.user_bar.size
		#sets variables with pos x y, and size x y of the user image
		size_x = interface.config.user_bar.size[0]
		size_y = interface.config.user_bar.size[1]*8/10
		pos_x = interface.config.user_bar.pos[0] + Window.size[0]/20
		pos_y = interface.config.user_bar.pos[1] + interface.config.user_bar.size[1]/10
		interface.config.ell.pos=(pos_x, pos_y)
		interface.config.ell.size=(size_y, size_y)




########################################################################################################################################
########################################################################################################################################
########################################################################################################################################


class WTApp(App):
    def build(self):
    	global control, interface, data
    	data = Model()
    	control = Controller()
    	interface = Interface()
    	return interface.layout

    def on_pause(self):
        return True

    def on_start(self):
        pass

#runs the app
if __name__ == '__main__':
    WTApp().run()