import kivy
kivy.require('1.1.1')


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty
from kivy.vector import Vector
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window, WindowBase
from kivy.config import Config

class Lt(Widget):
    def __init__(self, config):
        super(Lt, self).__init__()
        self.c = config
        self.l = FloatLayout()
        self.btn = Button(text='hello world')
        self.btn.bind(on_release=self.testt)
        self.l.add_widget(self.btn)

    def testt(self, instance):
        print self.c.test()

class Config(Widget):
    def test(self):
        return 'hello world' 
        

class WTApp(App):
    def build(self):
        t = Config()
        self.sm = Lt(t)
        return self.sm.l

    def on_pause(self):
        return True

    def on_start(self):
        pass
    

if __name__ == '__main__':
    WTApp().run()
