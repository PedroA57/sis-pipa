#mysql modules
#import mysql.connector
#from mysql.connector import *
#Tre4gh-l0l

#android modules
#from jnius import autoclass

#kivy modules
import kivy
kivy.require('1.1.1')


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty
from kivy.vector import Vector
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window, WindowBase
from kivy.config import Config

Config.set('graphics', 'width', '350')
Config.set('graphics', 'height', '600')
Config.set('graphics', 'fullscreen', 'fake')

#Window.size = (350, 600)


class Initial(Screen):
    def __init__(self, *args, **kwargs):
        super(Initial, self).__init__(*args, **kwargs)
        
        #layout of the screen
        self.l = FloatLayout()
        #background
        self.bck = Button(text='', size_hint=(1, 1), pos_hint={'x': 0, 'y':0}, disabled=True, background_disabled_normal='', background_color= (1, 1, 1, 1))
        #logo
        self.logo = Label(text='<LOGO>', pos_hint={'x': 0, 'y': .2}, color=(0, 0, 0, 1), font_size ='20sp')
        #username and passwords inputs
        #1: pc, 2: mobile
        self.image='/home/void/Desktop/Scripts/WTA/Images/ti.png'
        #self.image='ti.png'
        self.usernameinput = TextInput(hint_text='Usuario', text='', multiline=False, size_hint=(.8, .05), pos_hint={'x': .1, 'y': .4385}, border=(4, 4, 4, 4), background_normal = self.image, background_active = self.image)
        self.passwordinput = TextInput(hint_text='Senha', text='', multiline=False, size_hint=(.8, .05), pos_hint={'x': .1, 'y': .3885}, password=True, border=(4, 4, 4, 4), background_normal = self.image, background_active = self.image)
        self.lgin = Button(text='Login', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .22}, background_normal='', background_color= (.121, .458, 1, 1))
        self.signup = Button(text='Cadastre-se', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .11}, background_normal='', background_color= (.121, .458, 1, 1))
        
        #binds the callbacks
        self.lgin.bind(on_release=self.connect)
        self.usernameinput.bind(on_text_validate=self.connect)
        self.passwordinput.bind(on_text_validate=self.connect)

        #adds to the layout
        self.l.add_widget(self.bck)
        self.l.add_widget(self.passwordinput)
        self.l.add_widget(self.usernameinput)
        self.l.add_widget(self.logo)
        self.l.add_widget(self.lgin)
        self.l.add_widget(self.signup)
        self.add_widget(self.l)


    def connect(self, instance):
        #checks the login input
        
        #if its ok, goes to logged screen
        self.parent.transition.direction='left'
        self.parent.current='initialogged'


class Initialogged(Screen):
    def __init__(self, *args, **kwargs):
        super(Initialogged, self).__init__(*args, **kwargs)

        #layout of the screen
        self.l = FloatLayout()
        #background
        self.bck = Button(text='', size_hint=(1, 1), pos_hint={'x': 0, 'y':0}, disabled=True, background_disabled_normal='', background_color= (1, 1, 1, 1)) 
        #logo
        self.logo = Label(text='<LOGO>', pos_hint={'x': 0, 'y': .25}, color=(0, 0, 0, 1), font_size ='20sp')
        #get water button / pos_hint={'x': .25, 'y': .375(x 1.1)=.4125}
        self.getwater = Button(text='Pedir Agua', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .4125}, background_normal='', background_color= (.121, .458, 1, 1))
        #logout button / pos_hint={'x': .25, 'y': .2(x 1.1)=.22}
        self.logout = Button(text='Sair', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .22}, background_normal='', background_color= (.121, .458, 1, 1))
        
        
        #binds the callbacks
        self.getwater.bind(on_release=self.fgetwater)
        self.logout.bind(on_release=self.flogout)
        
        #adds to the layout
        self.l.add_widget(self.bck)
        self.l.add_widget(self.logo)
        self.l.add_widget(self.getwater)
        self.l.add_widget(self.logout)
        self.add_widget(self.l)        
            

    def fgetwater(self, instance):
        pass

    def flogout(self, instance):
        self.parent.logged=False
        self.parent.transition.direction='right'
        self.parent.current='initial'
        self.parent.children[0].usernameinput.text=''
        self.parent.children[0].passwordinput.text=''

    
class ConfigTab(Screen):
    def __init__(self, *args, **kwargs):
        super(ConfigTab, self).__init__(*args, **kwargs)
        #screen widgets
        self.l=FloatLayout(size_hint=(1, .9727))
        
        x = Window.size[0]
        y = Window.size[1]*95/100
        #user interface
        
        #accordion
        self.ac = Accordion(orientation='vertical',min_space=y/10, size_hint_y= .8)
        #accordion items
        self.userdata=AccordionItem(title='Dados Pessoais', min_space=(y*8/100), background_selected='atlas://data/images/defaulttheme/button')
        self.fornecedores = AccordionItem(title='Fornecedores', min_space=(y*8/100), background_selected='atlas://data/images/defaulttheme/button')
        self.dadosbancarios = AccordionItem(title='Dados Bancarios', min_space=(y*8/100), background_selected='atlas://data/images/defaulttheme/button')
        

        #adds the contents to each accordion item
        self.ud = ScrollView(do_scroll_x=False, size=(x, y*7/10), size_hint=(1, None))
        self.gl = GridLayout(cols=1, spacing=y/14, size_hint_y=None)
        print self.gl.pos
        self.gl.bind(minimum_height=self.gl.setter('height'))
        for i in range(30):
            if i == 0:
                b = Label(text=(''), size_hint=(1, None), height=y/20)
                self.gl.add_widget(b)
                continue
            
            if i == 1:
                b = Label(text=('Digite a nova informacao na entrada de texto correspondente.\nCaso nao deseje altera-la, deixe-a em branco.'), text_size=(x*9/10, None), halign='center', size_hint=(1, None), height=y/20)
                self.gl.add_widget(b)
                continue
            
            b = TextInput(text='', hint_text=('text input '+str(i+1)), size_hint=(1, None), height=y/20)
            self.gl.add_widget(b)

        self.ud.add_widget(self.gl)
        self.userdata.add_widget(self.ud)
        self.fornecedores.add_widget(Label(text='Hello World'))
        self.dadosbancarios.add_widget(Label(text='Hello World'))
        
        #adds each accordion item to the accordion itself
        self.ac.add_widget(self.userdata)
        self.ac.add_widget(self.fornecedores)
        self.ac.add_widget(self.dadosbancarios)
        
        #adds the accordion to the layout
        self.l.add_widget(self.ac)
        #adds the layout to te screen
        self.add_widget(self.l)

    
class SManager(ScreenManager):
    def __init__(self, *args, **kwargs):
        super(SManager, self).__init__(*args, **kwargs)
        self.size_hint=(1, .95)
        self.pos_hint={'x': 0, 'y': 0}
        self.logged=False
        self.add_widget(Initial(name='initial'))
        self.add_widget(Initialogged(name='initialogged'))
        self.add_widget(ConfigTab(name='configtab'))
        self.current='initial'
        

class Lt(Widget):
    def __init__(self, *args, **kwargs):
        super(Lt, self).__init__(*args, **kwargs)
        #variables
        self.actual=''
        self.cbar = False
        
        self.l=FloatLayout()
        #screens
        self.SM = SManager()
        #top bar
        self.bar = Button(text='', disabled = True, size_hint=(1, .1), pos_hint={'x':0, 'y': .95}, background_disabled_normal='', background_color= (.5, .5, .5, 1))
        #config Button / pos_hint={'x': .01, 'y': .98}
        sizey = Window.size[1]/20
        sizex = sizey*12/10
        #1: pc, 2: mobile
        self.image='/home/void/Desktop/Scripts/WTA/Images/bars_config.png'
        #self.image='bars_config.png'
        self.configbtn = Button(text='', size_hint=(None, None), size=(sizex, sizey), pos_hint={'x': 0, 'y': .95}, background_normal = self.image, background_down = self.image, border = (0, 0, 0, 0))

        #binds the callbacks
        self.configbtn.bind(on_release=self.config)
        
        #adds them to the layout
        self.l.add_widget(self.SM)
        self.l.add_widget(self.bar)
        self.l.add_widget(self.configbtn)

        
                


    def config(self, instance):
        if not self.cbar:
            allofit = str(self.SM.current_screen)
            x = len(allofit)-2
            self.actual = allofit[14:x]
            self.SM.transition=SlideTransition()
            self.SM.transition.direction='right'
            self.SM.current='configtab'
            self.cbar = True      
            
            
        elif self.cbar:
            self.SM.transition=SlideTransition()
            self.SM.transition.direction='left'
            self.SM.current=self.actual
            self.cbar = False
        
        
class WTApp(App):
    def build(self):
        self.sm = Lt()
        return self.sm.l

    def on_pause(self):
        return True

    def on_start(self):
        pass
    

if __name__ == '__main__':
    WTApp().run()
