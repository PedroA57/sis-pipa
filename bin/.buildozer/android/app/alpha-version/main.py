#mysql modules
import mysql.connector
from mysql.connector import *
#Tre4gh-l0l

#android modules
#from jnius import autoclass

#kivy modules
import kivy
kivy.require('1.1.1')


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty
from kivy.vector import Vector
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock

#Main interface of the app
class MainLayout(Widget):
    def __init__(self, *args, **kwargs):
        super(MainLayout, self).__init__(*args, **kwargs)
        #layout do app
        self.layout = FloatLayout()
        #class variables
            #loggin stats
        self.logged = False
        #count property for control configtab
        self.cnt = 0.0

        
        '''   
    #initial layout 
        #logo
        self.logo = Label(text='<LOGO>', pos_hint={'x': 0, 'y': .2}, color=(0, 0, 0, 1), font_size ='20sp')
        #background
        self.bck = Button(text='', size_hint=(1, 1), pos_hint={'x': 0, 'y':0}, disabled=True, background_disabled_normal='', background_color= (1, 1, 1, 1))
        #butao login
        self.login = Button(text='Login', size_hint=(.4, .05), pos_hint={'x': .3, 'y': .325}, background_normal='', background_color= (.121, .458, 1, 1))
        #butao cadastre-se
        self.signup = Button(text='Cadastre-se', size_hint=(.4, .05), pos_hint={'x': .3, 'y': .175}, background_normal='', background_color= (.121, .458, 1, 1))
        
        #adds the things to the main layout
        self.layout.add_widget(self.bck)
        self.layout.add_widget(self.logo)
        self.layout.add_widget(self.login)
        self.layout.add_widget(self.signup)
        
        #binds the callbacks to the buttons
        self.login.bind(on_release=self.login_screen)
        self.signup.bind(on_release=self.signup_screen)
        '''    
    #Initial layout
        #background
        self.bck = Button(text='', size_hint=(1, 1), pos_hint={'x': 0, 'y':0}, disabled=True, background_disabled_normal='', background_color= (1, 1, 1, 1))
        #logo
        self.logo = Label(text='<LOGO>', pos_hint={'x': 0, 'y': .2}, color=(0, 0, 0, 1), font_size ='20sp')
        #top bar
        self.bar = Button(text='', disabled = True, size_hint=(1, .07), pos_hint={'x':0, 'y': .93}, background_disabled_normal='', background_color= (.5, .5, .5, 1))
        #username and passwords inputs
        self.usernameinput = TextInput(hint_text='Usuario', text='', multiline=False, size_hint=(.8, .05), pos_hint={'x': .1, 'y': .399}, border=(4, 4, 4, 4), background_normal = '/home/void/Desktop/Luis_App/Images/ti.png', background_active = '/home/void/Desktop/Luis_App/Images/ti.png')
        self.passwordinput = TextInput(hint_text='Senha', text='', multiline=False, size_hint=(.8, .05), pos_hint={'x': .1, 'y': .35}, password=True, border=(4, 4, 4, 4), background_normal = '/home/void/Desktop/Luis_App/Images/ti.png', background_active = '/home/void/Desktop/Luis_App/Images/ti.png')
        self.lgin = Button(text='Login', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .2}, background_normal='', background_color= (.121, .458, 1, 1))
        self.signup = Button(text='Cadastre-se', size_hint=(.5, .05), pos_hint={'x': .25, 'y': .1}, background_normal='', background_color= (.121, .458, 1, 1))

        #binds the callbacks
        self.lgin.bind(on_release=self.connect)
        self.signup.bind(on_release=self.signup_screen)
        self.usernameinput.bind(on_text_validate=self.connect)
        self.passwordinput.bind(on_text_validate=self.connect)

        #adds to the layout
        self.layout.add_widget(self.bck)
        self.layout.add_widget(self.passwordinput)
        self.layout.add_widget(self.usernameinput)
        self.layout.add_widget(self.logo)
        self.layout.add_widget(self.lgin)
        self.layout.add_widget(self.signup)
        self.layout.add_widget(self.bar)

    #Initial Layout logged
        #get water button / pos_hint={'x': .25, 'y': .375}
        self.getwater = Button(text='Pedir Agua', size_hint=(.5, .05), pos_hint={'x': 1, 'y': .375}, background_normal='', background_color= (.121, .458, 1, 1))
        #logout button / pos_hint={'x': .25, 'y': .2}
        self.logout = Button(text='Sair', size_hint=(.5, .05), pos_hint={'x': 1, 'y': .2}, background_normal='', background_color= (.121, .458, 1, 1))
        #config Button / pos_hint={'x': .01, 'y': .98}
        self.configbtn = Button(text='', size_hint=(None, None), size=(40, 30), pos_hint={'x': .01, 'y': .94}, background_normal = '/home/void/Desktop/Scripts/WTA/Images/bars_config.png', background_down = '/home/void/Desktop/Scripts/WTA/Images/bars_config.png', border = (0, 0, 0, 0))
        #configbar
        self.configtab= Button(text='', size_hint=(1, 1), pos_hint={'x': -1, 'y':0}, disabled=True, background_disabled_normal='', background_color= (.4, .4, .4, .7))
        
        #binds the callbacks
        self.getwater.bind(on_release=self.fgetwater)
        self.logout.bind(on_release=self.initial_screen)
        self.configbtn.bind(on_release=self.config)

        #adds to the layout
        self.layout.add_widget(self.getwater)
        self.layout.add_widget(self.logout)
        self.layout.add_widget(self.configbtn)
        self.layout.add_widget(self.configtab)
        
        

    def connect(self, instance):
        #opens connection to the server
        
        #NOTE: database appserver doenst exists to user sispipa $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        
        #db = mysql.connector.connect(user='sispipa', password='Tre4gh-l0l', host='127.0.0.1', database='Appserver')
        '''ursor = db.cursor()
        #gets the usernames
        cursor.execute("SELECT * FROM Cliente_Usuario")
        for row in cursor.fetchall():
            print row
        
        #closes connections to the server
        '''
        #cheks if usernames and passwords are ok...
        if True and True: #True will be the boolean (username == <usernames on the server>) and True will be the boolean (password == <passwords on the server>)
            self.logged=True
            self.initial_logged_screen('')
            
        #db.close()
        
    #Screen methods
        
    def initial_screen(self, instance):
        #move the other things out of the screen
            #initial logged screen stuff
        self.getwater.pos_hint={'x': 1, 'y': .375}
        self.logout.pos_hint={'x': 1, 'y': .2}
            

        #move the login stuff in
            #login screen stuff
        self.logo.pos_hint={'x': 0, 'y': .2}
        self.usernameinput.pos_hint={'x': .1, 'y': .399}
        self.passwordinput.pos_hint={'x': .1, 'y': .35}
        self.signup.pos_hint={'x': .25, 'y': .1}
        self.lgin.pos_hint={'x': .25, 'y': .2}

        
    #Initial screens methods

        #config method
    def config(self, instance):
        #checks if the user is logged
        if not self.logged:
            #if don't, return
            return
        #if yes, call the function 50 times per second to gradually show the configtab
        Clock.schedule_interval(self.configbar, 0.01)
        
        
        #configbar method
    def configbar(self, dt):
        #adds the count to control the translation
        self.cnt = self.cnt + 0.024
        #ajust the x by the count
        x= -1.0 + self.cnt
        #change the configtab pos in x
        self.configtab.pos_hint={'x': x, 'y': 0}
        #checks if the tab is already at 80% of the screen
        if self.cnt >= 0.8:
            #if true, stop calling configbar function, stopping the configtab
            Clock.unschedule(self.configbar)
            
         
        
    def initial_logged_screen(self, instance):
        #move the other things out of the screen
            #login screen stuff
        self.usernameinput.pos_hint={'x': 1, 'y': .7}
        self.passwordinput.pos_hint={'x': 1, 'y': .5}
        self.lgin.pos_hint={'x': 1, 'y': .325}
        self.signup.pos_hint={'x': 1, 'y': .1}
        
        #move the initial logged screen stuff
            #initial logged screen stuff
        self.logo.pos_hint={'x': 0, 'y': .2}
        self.getwater.pos_hint={'x': .25, 'y': .375}
        self.logout.pos_hint={'x': .25, 'y': .2}
        

    #initial logged screens methods

    def fgetwater(self, instance):
        pass

    
    def signup_screen(self, instance):
        pass

        
class WTApp(App):
    def build(self):
        ml = MainLayout()
        l = ml.layout
        return l

    def on_pause(self):
        return True


if __name__ == '__main__':
    WTApp().run()
