'''
================================================================================================================================================================================================================================================
ABA DE CONFIGURACOES
================================================================================================================================================================================================================================================
'''
#import json functions
import json

#import kivy classes
from kivy.animation import Animation, AnimationTransition
from kivy.config import Config
from kivy.core.window import Window, WindowBase
from kivy.clock import Clock
from kivy.graphics import *
from kivy.properties import ObjectProperty
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from functools import partial

#import classes
from classes.alscreen import AlScreen


class ConfigTab(Widget):
	def __init__(self, control, idt, *args, **kwargs):
		#gets the screen class stuff
		#super(ConfigTab, self).__init__(*args, **kwargs)

		#define control
		self.control = control

		#x negativo para nao aparecer no visivel da tela
		x = Window.size[0]*(-0.8)
		#layout 
		self.layout=FloatLayout(size_hint=(.8, 1), pos_hint={'x':-0.8, 'y':0})
		self.layout.idt = idt
		#xy of the window
		x = Window.size[0]
		y = Window.size[1]

		#user interface
		self.user_bar=Widget(size_hint=(1, .4), pos_hint={'x': 0, 'y': .6})
		#adds the canvas instructions
		with self.user_bar.canvas:
			#lightsky color
			Color(.529, .807, .98, 1)
			#rectangle background
			self.rect = Rectangle(pos=self.user_bar.pos, size= self.user_bar.size)
			Color(1, 1, 1, 1)
			#Foto de perfil
			self.ell = Ellipse(source='/home/void/Desktop/Scripts/sis-pipa/Images/profile-42914_640.png', pos=self.user_bar.pos, size= self.user_bar.size)
		#User name on the interface:
		self.name = Label(text='[color=000000]Nome de Tal de Tal...[/color]', markup=True, pos_hint={'x':.02, 'y': .2}, font_size='18sp')
		#pegar self.name no banco de dados

		#updates the canvas instructions pos and size
		self.user_bar.bind(pos=self.canvas_update, size=self.canvas_update)

		#settings tabs:
		#self.userdata = Button(text='dados pessoais', size_hint=(1, .1), background_color=(1, 1, 1, 1), pos_hint={'x':0, 'y': .675})
		self.payment = Button(text='Pagamento', halign='left', valign='middle', size_hint=(1, .12), pos_hint={'x':0, 'y': .48}, background_normal='', background_color=(.168, .168, .168, 1))
		#allows the change of the pos of the button text
		self.payment.bind(size=self.payment.setter('text_size'))
		#self.payment.text_size=(Window.size[0]*0.8*0.4, Window.size[1]*0.12)
		
		self.pedidos = Button(text='Pedidos', halign='left', valign='middle', size_hint=(1, .12), pos_hint={'x': 0, 'y': .36}, background_normal='', background_color=(.168, .168, .168, 1))
		#allows the change of the pos of the button text
		self.pedidos.bind(size=self.pedidos.setter('text_size'))

		self.help = Button(text='Ajuda', halign='left', valign='middle', size_hint=(1, .12), pos_hint={'x': 0, 'y': .24}, background_normal='', background_color=(.168, .168, .168, 1))
		#allows the change of the pos of the button text
		self.help.bind(size=self.help.setter('text_size'))

		self.settings = Button(text='Configuracoes', halign='left', valign='middle', size_hint=(1, .12), pos_hint={'x': 0, 'y': .12}, background_normal='', background_color=(.168, .168, .168, 1))
		#allows the change of the pos of the button textbackground_normal='', 
		self.settings.bind(size=self.settings.setter('text_size'))
		
		#binds the callback
		#carrega o json file na variavel self.jsn e passa como parametro da funcao
		with open("/home/void/Desktop/Scripts/sis-pipa/data/classes_jsons/test.json") as json_file:
			self.jsn = json.load(json_file)
		#binds the callbacks
		self.payment.bind(on_release=partial(self.opn, self.jsn, 0.6))
		self.pedidos.bind(on_release=partial(self.opn, self.jsn, 0.6))
		self.help.bind(on_release=partial(self.opn, self.jsn, 0.6))
		self.settings.bind(on_release=partial(self.opn, self.jsn, 0.6))
		
		#adds the things to the layout
		self.layout.add_widget(self.user_bar)
		self.layout.add_widget(self.name)
		self.layout.add_widget(self.payment)
		self.layout.add_widget(self.pedidos)
		self.layout.add_widget(self.help)
		self.layout.add_widget(self.settings)
		#cria os obejetos tela de alteracao de dados
		#self.scr = None

	#updates the canvas pos and size
	def canvas_update(self, *args):
		#updates background pos and size
		self.rect.pos=self.user_bar.pos
		self.rect.size=self.user_bar.size
		#sets variables with pos x y, and size x y of the user image
		size = self.user_bar.size[1]/2
		pos_x = self.user_bar.pos[0] + (Window.size[0]*0.8)/2 - size/2
		#pos y e os pos da barra + metade do tamanho y da barra nao ocupado pelo circulo + tamanho do nome( [10/10 - 4/10]/2 + 2/10 => 5/10 => 0.5)
		pos_y = self.user_bar.pos[1] + self.user_bar.size[1]*0.4
		self.ell.pos=(pos_x, pos_y)
		self.ell.size=(size, size)


	#opens the screens
	def opn(self, jsn, t, instance):
		#desabilita os botoes na aba de configuracoes
		self.payment.disabled = True
		self.pedidos.disabled = True
		self.help.disabled = True
		self.settings.disabled = True
		#cria a tela
		self.scr = AlScreen(control=self.control, jsn=jsn, idt='AlScreen')
		#adicona ela
		self.layout.parent.add_widget(self.scr)
		#Abre ela
		#cria a animacao
		opn = Animation(x=0, y=0, t='out_quint', duration=t)
		#inicia a animacao
		opn.start(self.scr)
		#self.scr.pos=(10, 10)

	#closes the screens
	def cls(self, scr, instance):
		pass

