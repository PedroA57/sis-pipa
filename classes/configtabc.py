'''
================================================================================================================================================================================================================================================
ABA DE CONFIGURACOES DO CONSUMIDOR
================================================================================================================================================================================================================================================
'''

#import kivy classes
from kivy.animation import Animation, AnimationTransition
from kivy.config import Config
from kivy.core.window import Window, WindowBase
from kivy.clock import Clock
from kivy.graphics import *
from kivy.logger import Logger
from kivy.properties import ObjectProperty, ListProperty
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from functools import partial

#import classes
from classes.entrega import Entrega
from classes.cartao import Cartao
from classes.pedidos import Pedidos
from classes.dados_pessoais import Dados_Pessoais


class ConfigTabC(Widget):
	def __init__(self, control, idt, db, *args, **kwargs):
		
		#define control
		self.control = control
		#define its id
		self.idt = idt
		#define server connection
		self.db = db

		'''
		#gets consumidor's id
		#creates a querry
		querry = 'SELECT id FROM Usuario WHERE id_Usuario="%s";' % (str(control.model.userid))
		#executes the querry and gets the cursor
		cursor = db.querry(querry)
		#gets the results form the querry within the cursor
		results = cursor.fetchall()
		#checks if it gets more than one id
		if len(results) > 1:
			Logger.warning('Server: Error when trying to get user id')
		#if not
		elif:
			self.userid = results[0]
		'''

		#x negativo para nao aparecer no visivel da tela
		x = Window.size[0]*(-0.8)
		#layout 
		self.layout=FloatLayout(size_hint=(.8, 1), pos_hint={'x':-0.8, 'y':0})
		self.layout.idt = idt
		#xy of the window
		x = Window.size[0]
		y = Window.size[1]

		#button on the side to close configtab
		self.sidebtn = Button(text='', size_hint=(.25, 1), pos_hint={'x': -1, 'y': 0}, background_color=(1, 0, 0, 0), background_normal='')

		#user interface
		self.user_bar=Widget(size_hint=(1, .4), pos_hint={'x': 0, 'y': .6})
		#adds the canvas instructions
		#User name on the interface:
		self.name = Label(text='[color=121212]'+self.control.model.user.nome+'[/color]', markup=True, pos_hint={'x':.02, 'y': .2}, font_size='18sp')
		#pegar self.name no banco de dados


		#settings tabs:
		#scrollview
		self.scv = ScrollView(do_scroll_x=False, size_hint=(1, None), size=(Window.width, Window.height*0.6), pos_hint={'x': 0, 'y': 0})
		#grid layout of all the buttons
		self.grd =  GridLayout(size_hint=(1, None), cols=1)

		#self.userdata = Button(text='dados pessoais', size_hint=(1, .1), background_color=(1, 1, 1, 1), pos_hint={'x':0, 'y': .675})
		self.payment = Button(text='Pagamento', halign='left', valign='middle', height=Window.height*0.09, size_hint=(1, None), pos_hint={'x':0, 'y': None}, background_normal='', background_color=(.168, .168, .168, 0))
		#allows the change of the pos of the button text .12 .48
		self.payment.bind(size=partial(self.btntext, self.payment))
		
		self.pedidos = Button(text='Pedidos', halign='left', valign='middle', height=Window.height*0.09, size_hint=(1, None), pos_hint={'x': 0, 'y': None}, background_normal='', background_color=(.168, .168, .168, 0))
		#allows the change of the pos of the button text .12 .36
		self.pedidos.bind(size=partial(self.btntext, self.pedidos))
	
		self.dadosp = Button(text='Dados Pessoais', halign='left', valign='middle', height=Window.height*0.09, size_hint=(1, None), pos_hint={'x': 0, 'y': None}, background_normal='', background_color=(.168, .168, .168, 0))
		#allows the change of the pos of the button text .12 .24
		self.dadosp.bind(size=partial(self.btntext, self.dadosp))

		self.fornecedores = Button(text='Fornecedores', halign='left', valign='middle', height=Window.height*0.09, size_hint=(1, None), pos_hint={'x': 0, 'y': None}, background_normal='', background_color=(.168, .168, .168, 0))
		#allows the change of the pos of the button text .12 .12 
		self.fornecedores.bind(size=partial(self.btntext, self.fornecedores))

		self.entrega = Button(text='Endereco de Entrega', halign='left', valign='middle', height=Window.height*0.09, size_hint=(1, None), pos_hint={'x': 0, 'y': None}, background_normal='', background_color=(.168, .168, .168, 0))
		#allows the change of the pos of the button text .12 .12 
		self.entrega.bind(size=partial(self.btntext, self.entrega))
		

		#binds the callbacks
		#closes th configtab
		self.sidebtn.bind(on_press=partial(self.control.config, .85))
		#each one opens their own tab
		self.payment.bind(on_release=self.fpayment)
		self.pedidos.bind(on_press=self.fpedidos)
		self.dadosp.bind(on_press=self.fdadosp)
		self.fornecedores.bind(on_press=self.ffornecedores)
		self.entrega.bind(on_press=self.fentrega)
	
		#adds them
		#add buttons to the grid layout
		self.grd.add_widget(self.payment)
		self.grd.add_widget(self.pedidos)
		self.grd.add_widget(self.dadosp)
		self.grd.add_widget(self.fornecedores)
		self.grd.add_widget(self.entrega)
		#add the grid layout to the scroll view widget
		self.scv.add_widget(self.grd)
		#adds them to the layout
		self.layout.add_widget(self.user_bar)
		self.layout.add_widget(self.name)
		self.layout.add_widget(self.scv)
		#adds the side button
		self.layout.add_widget(self.sidebtn)
		#cria os obejetos tela de alteracao de dados
		#self.scr = None


	def fentrega(self, instance):
		#cria a tela
		self.scr = Entrega(control=self.control, idt='AlScreen', db=self.db)
		#cria a animacao
		opn = Animation(x=0, y=0, t='out_quint', duration=0.4)
		#call the method to open it
		self.opn(opn)
		

	def fpayment(self, instance):
		#cria a tela
		self.scr = Cartao(control=self.control, idt='AlScreen', db=self.db)
		#cria a animacao
		opn = Animation(x=0, y=0, t='out_quint', duration=0.4)
		#call the method to open it
		self.opn(opn)

	def fpedidos(self, instance):
		#cria a tela
		self.scr = Pedidos(control=self.control, idt='AlScreen', db=self.db)
		#cria a animacao
		opn = Animation(x=0, y=0, t='out_quint', duration=0.4)
		#bind the animation to when its done, ajust filter pos
		opn.bind(on_complete=self.scr.ajustf)
		#call the method to open it
		self.opn(opn)

	def fdadosp(self, instance):
		#cria a tela
		self.scr = Dados_Pessoais(control=self.control, idt='AlScreen', db=self.db)
		#cria a animacao
		opn = Animation(x=0, y=0, t='out_quint', duration=0.4)
		#call the method to open it
		self.opn(opn)

	def ffornecedores(self, instance):
		return
		#cria a tela
		self.scr = Entrega(control=self.control, idt='AlScreen', db=self.db)
		#cria a animacao
		opn = Animation(x=0, y=0, t='out_quint', duration=0.4)
		#call the method to open it
		self.opn(opn)

	#opens the screen
	def opn(self, opn):
		#desabilita os botoes na aba de configuracoes
		self.payment.disabled = True
		self.pedidos.disabled = True
		self.dadosp.disabled = True
		self.fornecedores.disabled = True
		self.entrega.disbaled = True
		#adicona ela
		self.layout.parent.add_widget(self.scr)
		#binds the returnbtn to close alscreen
		self.control.interface.returnbtn.bind(on_release=self.clse)
		#close configtab
		self.control.config(0.05, False)
		#updates the canvas
		self.canvas_update()
		#Abre  a tela
		#inicia a animacao
		opn.start(self.scr)
		#move the config button out of the screen
		self.control.interface.configbtn.pos_hint={'x': 1, 'y': 0.95}
		#puts return button in the up bar
		self.control.interface.returnbtn.pos_hint={'x': 0, 'y': 0.95}

	#closes the screen
	def clse(self, instance):
		#unbinds return button to clse, so it wont be binded over and over, junt once per use
		self.control.interface.returnbtn.unbind(on_release=self.clse)
		#move return button out of the screen
		self.control.interface.returnbtn.pos_hint={'x': 1, 'y': 0.95}
		#move the config button to the screen again
		self.control.interface.configbtn.pos_hint={'x': 0, 'y': 0.95}
		#enables configs button
		self.payment.disabled = False
		self.pedidos.disabled = False
		self.dadosp.disabled = False
		self.fornecedores.disabled = False
		self.entrega.disbaled = False
		#creates the closing animation
		clse = Animation(x=0, y=Window.size[1]*(-1), t='out_quint', duration=0.4)
		#binds to clse to on complete remove Alscreen
		clse.bind(on_complete=self.remve)
		#start animation
		clse.start(self.scr)
		

	#method to remove AlScreen, to be called when closing animation is done, so only when its done AlScreen will be removed 
	def remve(self, t, instance):
		#removes alscreen
		self.layout.parent.remove_widget(self.scr)
		#erase any trace to alscreen
		#self.scr = None


	#move the button's text to the left
	def btntext(self, instance, v, size):
		instance.text_size = (Window.size[0]*0.7, Window.size[1]*0.09)


	#updates the canvas pos and size
	def canvas_update(self, *args):
		#updates background pos and size
		self.rect.pos=self.layout.pos
		self.rect.size=self.layout.size
		self.rect2.pos=self.layout.pos 
		self.rect2.size=(self.layout.size[0], self.layout.size[1]*0.6)
		#sets variables with pos x y, and size x y of the user image
		size = self.user_bar.size[1]/2
		pos_x = self.layout.pos[0] + (Window.size[0]*0.8)/2 - size/2
		#pos y e os pos da barra + metade do tamanho y da barra nao ocupado pelo circulo + tamanho do nome( [10/10 - 4/10]/2 + 2/10 => 5/10 => 0.5)
		pos_y = self.user_bar.pos[1] + self.user_bar.size[1]*0.4
		self.ell.pos=(pos_x, pos_y)
		self.ell.size=(size, size)

	#mwthod to create the canvas
	def create_canvas(self):
		with self.layout.canvas.before:
			#lightsky color
			Color(.529, .807, .98, 1)
			#rectangle background
			self.rect = Rectangle(pos=self.user_bar.pos, size= self.user_bar.size)
			#color
			Color(.168, .168, .168, 1)
			#bottom rectangle background
			self.rect2 = Rectangle(pos=self.layout.pos, size=(self.layout.size[0], self.layout.size[1]*0.6))
			Color(1, 1, 1, 1)
			#Foto de perfil
			self.ell = Ellipse(source='Images/profile-42914_640.png', pos=self.user_bar.pos, size= self.user_bar.size)
		#updates the canvas instructions pos and size
		self.layout.bind(pos=self.canvas_update, size=self.canvas_update)

	#method to clear the canvas
	def clear_canvas(self, instance):
		self.layout.canvas.clear()


