'''
================================================================================================================================================================================================================================================
TELA INICIAL DO FORNECEDOR
================================================================================================================================================================================================================================================
'''

from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window, WindowBase
from kivy.config import Config
from kivy.animation import Animation, AnimationTransition
from kivy.graphics import *

class Initialogged_forn(Screen):
	def __init__(self, control, *args, **kwargs):
		super(Initialogged_forn, self).__init__(*args, **kwargs)

		#the main layout
		self.layout = FloatLayout()
		
		#adds the layout to te screen
		self.add_widget(self.layout)