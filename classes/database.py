'''
================================================================================================================================================================================================================================================
CLASSE DE CONEXAO COM O SERVIDOR
================================================================================================================================================================================================================================================
'''

#import mysql
import mysql.connector
#import kivy logger
from kivy.logger import Logger

#db connection class
class Database:
	conn = None

	def __init__(self, host, user, password, instance, db):
		self.host = host
		self.user = user
		self.password = password
		self.db = db
		self.instance = instance

	#connection method
	def connect(self):
		try:
			#connections to the server with the host, user, password, databases as the parameters
			self.conn = mysql.connector.connect(host=self.host, user=self.user, password=self.password, database=self.db)
			#informs that the conections was established
			#Logger.info('Server: Connection successful')
			#return true
			return True
		except:
			#warns that the connection failed
			#Logger.warning('Server: Connection failed')
			#return false
			return 'Connection Failed'

	#method to return cursor
	def get_cursor(self):
		try:
			#returns the cursor
			cursor = self.conn.cursor()

			return cursor

		except:
			#if raises a error, establish the connection first
			#creates a connection
			if self.connect() == "Connection Failed":
				return None
			#then returns the cursor
			return self.conn.cursor()

	def select(self, sql):
		try:
			#creates a cursor
			cursor = self.conn.cursor()
			#executes the sql
			cursor.execute(sql)
		#if it raises a error, creates a connection with self.connect()
		except:
			#creates a connection
			if self.connect() == "Connection Failed":
				return None
			#creates a cursor
			cursor = self.conn.cursor()
			#executes the querry
			cursor.execute(sql)
		#return the cursor
		return cursor

	#method to do a select querry
	def query(self, sql, data):
		#try execute a querry
		try:
			#creates a cursor
			cursor = self.conn.cursor()
			#executes the sql
			cursor.execute(sql, data)
		#if it raises a error, creates a connection with self.connect()
		except:
			#creates a connection
			if self.connect() == "Connection Failed":
				return None
			#creates a cursor
			cursor = self.conn.cursor()
			#executes the querry
			cursor.execute(sql, data)
		#return the cursor
		return cursor

	#method to do a insert querry
	def data_query(self, sql, data):
		#try execute a querry
		try:
			#creates a cursor
			cursor = self.conn.cursor()
			#executes the sql
			cursor.execute(sql, data)
			#commits
			self.conn.commit()
			#closes cursor
			cursor.close()
		#if it raises a error, creates a connection with self.connect()
		except:
			#creates a connection
			if self.connect() == "Connection Failed":
				return None
			#creates a cursor
			cursor = self.conn.cursor()
			#executes the querry
			cursor.execute(sql, data)
			#commits
			self.conn.commit()
			#closes cursor
			cursor.close()

	#method to close connected
	def close_connection(self):
		try:
			self.conn.close()
		except:
			pass
		
