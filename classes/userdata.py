from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window, WindowBase
from kivy.config import Config
from kivy.animation import Animation, AnimationTransition
from kivy.graphics import *

class Userdata(Widget):
	def __init__(self, control, *args, **kwargs):
		super(Userdata, self).__init__(*args, **kwargs)
		#the main layout
		x = Window.size[0]
		y = Window.size[1]*0.95
		z = Window.size[1]*(-1)
		self.layout = FloatLayout(pos=(0, z), size=(x, y))
		#o pos hint ta em -1 para ele n aparecer no visivel da tela

		#button to change dados pessoais
		x = Window.size[0]
		y = Window.size[1]/10
		#dados pessoais button
		self.userdatabtn = Button(text='Dados pessoais', size_hint=(1, .1), pos_hint={'x': 0, 'y': .9})
		#background
		self.bckgrnd = Button(pos_hint={'x': 0, 'y': 0}, text='', disabled=True, background_disabled_normal='', background_disabled_down='', size_hint=(1, 1), background_color=(1, 1, 1, 1))

		#adds tem to the layout
		self.layout.add_widget(self.bckgrnd)
		self.layout.add_widget(self.userdatabtn)
		
		#adds the layout to te screen
		self.add_widget(self.layout)