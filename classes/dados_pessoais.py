#import kivy modules
from kivy.animation import Animation, AnimationTransition
from kivy.config import Config
from kivy.core.window import Window, WindowBase
from kivy.clock import Clock
from kivy.graphics import *
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.metrics import Metrics
from functools import partial

import mysql.connector
import datetime


class Dados_Pessoais(FloatLayout):
	def __init__(self, control, db, idt, *args, **kwargs):
		super(Dados_Pessoais, self).__init__(*args, **kwargs)
		#defines control class
		self.control = control
		#define idt
		self.idt = idt
		#assigns connection to the server to the db
		self.db = db
		#pos and size of the screen
		self.pos=(0, Window.size[1]*(-1))
		self.size=(Window.size[0], Window.size[1]*0.95)

		#creates screen stuff

		#scroll view
		self.scrol_view = ScrollView(do_scroll_x=False, size_hint=(1, None), size=(Window.width, Window.height*0.95*0.9), pos_hint={'x': 0, 'y': .1})
		#create grid layout
		grd = GridLayout(size_hint=(1, None), cols=1, spacing=Window.size[1]/35)
		grd.bind(minimum_height=grd.setter('height'))
		#crete the box layouts with the textinputs in it

		#image of text input
		image = 'Images/ti.png'
		#user class
		user = self.control.model.user

		#box0:
		b0 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/30, orientation='vertical')
		t0 = Button(text='', disbaled=True, size_hint=(.9, .5), pos_hint={'x': .05, 'y': 0}, background_normal='', background_selected = '', background_color=(1, 1, 1, 0))
		b0.add_widget(t0)
		grd.add_widget(b0)

		#box1: (the others follows this model, so they wont be commented)
		#the box layout that include the text input and their specification label
		b1 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/10, orientation='vertical')
		#the specification label
		l1 = Label(text='Nome:', halign='left', valign='middle', size_hint=(1, .5), pos_hint={'x': .05, 'y': 0})
		l1.text_size=(Window.size[0], Window.size[1]/10)
		#te text input
		self.t1 = TextInput(text=user.nome, multiline=False, size_hint=(.9, .5), font_size=(15*(float(Window.height)/500.0)), hint_text_color=(1, 0, 0, 1), pos_hint={'x': .05, 'y': 0}, background_normal=image, background_selected = image)
		#adds them
		b1.add_widget(l1)
		b1.add_widget(self.t1)
		grd.add_widget(b1)

		#box2:
		b2 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/10, orientation='vertical')
		l2 = Label(text='Email:', halign='left', valign='middle', size_hint=(1, .5), pos_hint={'x': .05, 'y': 0})
		l2.text_size=(Window.size[0], Window.size[1]/10)
		self.t2 = TextInput(text=user.login, multiline=False, size_hint=(.9, .5), font_size=(15*(float(Window.height)/500.0)), hint_text_color=(1, 0, 0, 1), pos_hint={'x': .05, 'y': 0}, background_normal=image, background_selected = image)
		b2.add_widget(l2)
		b2.add_widget(self.t2)
		grd.add_widget(b2)

		#box3:
		b3 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/10, orientation='vertical')
		l3 = Label(text='CPF: (Digite somente numeros)', halign='left', valign='middle', size_hint=(1, .5), pos_hint={'x': .05, 'y': 0})
		l3.text_size=(Window.size[0], Window.size[1]/10)
		self.t3 = TextInput(text=user.cpf, multiline=False, size_hint=(.9, .5), font_size=(15*(float(Window.height)/500.0)), hint_text_color=(1, 0, 0, 1), pos_hint={'x': .05, 'y': 0}, background_normal=image, background_selected = image)
		b3.add_widget(l3)
		b3.add_widget(self.t3)
		grd.add_widget(b3)

		#box4: 
		b4 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/10, orientation='vertical')
		l4 = Label(text='Senha:', halign='left', valign='middle', size_hint=(1, .5), pos_hint={'x': .05, 'y': 0})
		l4.text_size=(Window.size[0], Window.size[1]/10)
		self.t4 = TextInput(text='', password=True, multiline=False, size_hint=(.9, .5), font_size=(15*(float(Window.height)/500.0)), hint_text_color=(1, 0, 0, 1), pos_hint={'x': .05, 'y': 0}, background_normal=image, background_selected = image)
		b4.add_widget(l4)
		b4.add_widget(self.t4)
		grd.add_widget(b4)

		#box5: 
		b5 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/10, orientation='vertical')
		l5 = Label(text='Confirme a Senha:', halign='left', valign='middle', size_hint=(1, .5), pos_hint={'x': .05, 'y': 0})
		l5.text_size=(Window.size[0], Window.size[1]/10)
		self.t5 = TextInput(text='', password=True, multiline=False, size_hint=(.9, .5), font_size=(15*(float(Window.height)/500.0)), hint_text_color=(1, 0, 0, 1), pos_hint={'x': .05, 'y': 0}, background_normal=image, background_selected = image)
		b5.add_widget(l5)
		b5.add_widget(self.t5)
		grd.add_widget(b5)

		#box6
		b6 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/10, orientation='vertical')
		blank = Widget(size_hint=(1, 1))
		delete_btn = Button(text='Deletar Conta', size_hint=(.5, 1), pos_hint={'x': .25, 'y': 0}, background_color=(0.659, 0.114, 0.098, 1), background_normal='')
		b6.add_widget(blank)
		b6.add_widget(delete_btn)
		grd.add_widget(b6)

		#box9(outside Grid Layout): 
		last_box = BoxLayout(size_hint=(1, .1), orientation='horizontal')
		self.btn_cancel = Button(text='Cancelar', size_hint=(.5, 1), pos_hint={'x': 0, 'y': 0}, background_color=(0.659, 0.114, 0.098, 1), background_normal='')
		self.btn_confirm = Button(text='Confirmar', size_hint=(.5, 1), pos_hint={'x': 0, 'y': 0}, background_color=(0.518, 0.643, 0.933, 1), background_normal='')
		self.btn_cancel.bind(on_release=self.control.interface.screens['ConfigTab'].clse)
		self.btn_confirm.bind(on_release=self.opn_popup)
		last_box.add_widget(self.btn_cancel)
		last_box.add_widget(self.btn_confirm)


		#binds the callback
		#binds next_one method to the textinput to move the cursor when enter is pressed
		self.t1.bind(focus=self.reset_hint_text)
		self.t2.bind(focus=self.reset_hint_text)
		self.t3.bind(focus=self.reset_hint_text)
		self.t4.bind(focus=self.reset_hint_text)
		self.t5.bind(focus=self.reset_hint_text)
		self.t1.bind(on_text_validate=partial(self.next_one, self.t2))
		self.t2.bind(on_text_validate=partial(self.next_one, self.t3))
		self.t3.bind(on_text_validate=partial(self.next_one, self.t4))
		self.t4.bind(on_text_validate=partial(self.next_one, self.t5))
		self.t5.bind(on_text_validate=self.opn_popup)
		delete_btn.bind(on_release=partial(self.delete_popup, 1, None))
		

		#adds them to the screen
		self.scrol_view.add_widget(grd)
		self.add_widget(last_box)
		self.add_widget(self.scrol_view)

		#create canvas
		self.create_canvas()

	#method to open delete popup
	def delete_popup(self, ntry, pop, instance):
		#its content
		content = FloatLayout()
		#buttons of the content
		btn_cancel = Button(text='Cancelar', size_hint=(.4, .4), pos_hint={'x': .05, 'y': .3}, background_color=(0.659, 0.114, 0.098, 1), background_normal='')
		btn_confirm = Button(text='Confirmar', size_hint=(.4, .4), pos_hint={'x': .55, 'y': .3}, background_color=(0.518, 0.643, 0.933, 1), background_normal='')
		#adds them
		content.add_widget(btn_cancel)
		content.add_widget(btn_confirm)

		#creates the popup
		pop = Popup(title='Uma vez deletada, nenhum dado\npodera ser restaurado, ainda deseja\ndeletar sua conta?', content=content, auto_dismiss=True, size_hint=(.95, .36), title_align='center')

		#binds the buttons callbacks, down here so the dismiss method can be binded, and to do that the pop has to be already declared
		btn_cancel.bind(on_release=pop.dismiss)
		btn_confirm.bind(on_release=partial(self.delete_user, pop))
		#open popup
		pop.open()

	def delete_user(self, pop, instance):
		pop.dismiss()
		#delete user
		sql="""DELETE FROM Usuario WHERE id=%s"""
		#user id
		data=(self.control.model.user.idt, )
		#execute the sql
		self.control.db.data_query(sql, data)
		#warns the user

		#logout (screen, instance)
		self.control.logout('initialogged_user', pop)

	#method to open confirmation popup
	def opn_popup(self, instance):
		#its content
		content = FloatLayout()
		#buttons of the content
		btn_cancel = Button(text='Cancelar', size_hint=(.4, .4), pos_hint={'x': .05, 'y': .3}, background_color=(0.659, 0.114, 0.098, 1), background_normal='')
		btn_confirm = Button(text='Confirmar', size_hint=(.4, .4), pos_hint={'x': .55, 'y': .3}, background_color=(0.518, 0.643, 0.933, 1), background_normal='')
		#adds them
		content.add_widget(btn_cancel)
		content.add_widget(btn_confirm)

		#creates the popup
		pop = Popup(title='Voce Realmente deseja\n  Alterar os dados?  ', content=content, auto_dismiss=True, size_hint=(.95, .3), title_align='center', )

		#binds the buttons callbacks, down here so the dismiss method can be binded, and to do that the pop has to be already declared
		btn_cancel.bind(on_release=self.control.interface.screens['ConfigTab'].clse)
		btn_cancel.bind(on_release=pop.dismiss)
		btn_confirm.bind(on_release=partial(self.submit, pop))
		## Device independent pixels 
		#px = dp * density 
		## Scale independent pixels 
		#px = sp * density * fontscale 
		## Points 
		#px = pt * dpi / 72

		#pen popup
		pop.open()

	#method to register the data in the database
	def submit(self, pop, instance):
		#closes popup
		pop.dismiss()
		#checks the inputs
		if not self.check():
			#if any isnt good, dont updates the server
			return
	
		#register in the database...
		#user stores the user data
		user = self.control.model.user

		#time this is updated
		now = datetime.date.today()
		#checks the empty inputs, those wont be added to the server
		if (self.t4.text=='') and (self.t5.text==''):
			#order of the data to be updated: login, nome
			data=(self.t2.text, self.t1.text, now, user.idt)
			sql=("""UPDATE Usuario SET login=%s, nome=%s, atualizado_em=%s WHERE id=%s""")
		else:
			#order of the data to be updated: login, senha, nome
			data=(self.t2.text, self.t4.text, self.t1.text, now, user.idt)
			sql=("""UPDATE Usuario SET login=%s, senha=%s, nome=%s, atualizado_em=%s WHERE id=%s""")

		#run the sql
		self.control.db.data_query(sql, data)

		#update user instance
		user.nome = self.t2.text
		user.login = self.t1.text
		user.atualizado_em = now

		#notificate the user

		#calls configtabc method to close tab
		self.control.interface.screens['ConfigTab'].clse(instance)
		print 'done'

	#method to check user inputs
	def check(self):
		print 'is checking'
		#checks if the user names is blank
		if self.t1.text=='':
			self.t1.hint_text='Nome Invalido'
			self.t1.text=''
			return False

		#checks if the user login is blank
		if self.t2.text=='':
			self.t2.hint_text='Email Invalido Invalido'
			self.t2.text=''
			return False
			
		#checks if the passwords has at least 7 characters
		if (len(self.t4.text)<=6) and (self.t4.text!=''):
			self.t4.hint_text='A Senha precisa ter no minimo 7 digitos'
			self.t4.text=''
			self.t4.password = False
			self.t4.focus = False
			return False

		#checks if the password has a space
		if not(self.t4.text.find(' ') == -1):
			self.t4.hint_text='Senha Invalida'
			self.t4.text=''
			print 'here'

		#checks if the passwords matches its confirmation
		elif not(self.t4.text == self.t5.text):
			self.t4.hint_text='Senhas nao identicas'
			self.t5.hint_text='Senhas nao identicas'
			self.t4.text=''
			self.t5.text=''
			self.t4.password=False
			self.t5.password=False
			self.t4.focus = False
			self.t5.focus = False
			return False

		#checks the login consistence, if it has '@' and '.com'
		if (('@' not in self.t2.text) or ('.com' not in self.t2.text)) and (self.t2.text!=''):
			self.t2.hint_text='Email Invalido'
			self.t2.text=''
			self.t2.focus = False
			return False

		#checks if the CPF/CNPJ is a number
		#try:
		#	int(self.t10.text)
		#except:
		#	self.t10.hint_text='Digite um "%s" valido' % (self.cpf_cnpj)
		#	self.t10.text=''
		#	self.t10.focus = False
		#	return

		#checks if it is a valid cpf/cnpj
		elif (len(self.t3.text) != 11) and (self.t3.text!=''):
			self.t3.hint_text='Digite um CPF valido'
			self.t3.text=''
			self.t3.focus = False
			return False

		#if everything is ok, return true
		else:
			return True

	#method to move the cursor to the next textinput
	def next_one(self, next_one, instance):
		#move the cursor to the next texinput
		next_one.focus = True


	#updates canvas
	def create_canvas(self):
		#adds Canvas instructions
		with self.canvas.before:
			#background color
			Color(.168, .168, .168, 1)
			#background itself
			self.rect = Rectangle(pos=self.scrol_view.pos, size=self.scrol_view.pos)

		#update the canvas instruction
		self.bind(pos=self.update_canvas, size=self.update_canvas)


	#atualiza a posicao e tamanho da instrucao do canvas
	def update_canvas(self, *args):
		#atualiza a pos
		self.rect.pos=self.scrol_view.pos
		#atualiza o tamanho
		self.rect.size=self.scrol_view.size

	#method to reset hint text
	def reset_hint_text(self, instance, value):
		if value:
			instance.hint_text=''


	def dl(self, instance):
		#habilita os botoes da aba de configuracao
		l = instance.parent.parent.children
		# configtab layout instance
		c = None
		#get the configtab layout instance
		for a in l:
			if a.idt == 'ConfigTab':
				#assigns it to variable c (c=configtab.layout)
				c = a

		#runs for every button in c.scv.grd.children(configtab.layout.scv.grd.children=childs of the gridlayout within the scrollview within the configtab.layout)
		for b in c.scv.grd.children:
			#enables it beacause they were disbaled when the ascreen layout was open
			b.disabled = False
		
		#destroi a janela
		instance.parent.parent.remove_widget(instance.parent)