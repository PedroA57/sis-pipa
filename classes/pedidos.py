'''
================================================================================================================================================================================================================================================
TELA DE DADOS BANCARIOS DO CONSUMIDOR
================================================================================================================================================================================================================================================
'''


#import kivy modules
from kivy.animation import Animation, AnimationTransition
from kivy.config import Config
from kivy.core.window import Window, WindowBase
from kivy.clock import Clock
from kivy.graphics import *
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from functools import partial


class Pedidos(FloatLayout):
	def __init__(self, control, db, idt, *args, **kwargs):
		super(Pedidos, self).__init__(*args, **kwargs)
		#defines control class
		self.control = control
		#defines its id
		self.idt = idt
		#defines the server connection
		self.db = db
		#to control if filter layout is either open or close
		self.ctrl = False
		#list com os pedidos obtidos do banco de dados
		self.pedidos = []

		self.pos=(0, Window.size[1]*(-1))
		self.size=(Window.size[0], Window.size[1]*0.1)


		#screen stuff
		#filter layout
		self.flayout = FloatLayout(size_hint=(1, 0.4), pos_hint={'x': 1, 'y': 2})

		#user pedidos layout
		self.uplayout =FloatLayout(size_hint=(1, 0.95), pos_hint={'x': 0, 'y': 0})
		#button to close/open filter tab
		
		#self.ocbtn = Button(text='Pesquisa Avancada', size_hint=(1, .05), pos_hint={'x': 0, 'y': 0.95}, background_color=(1, 1, 1, 1), color=(0, 0, 0, 1), background_normal='', background_down='')
		
		#scrrolview
		scv = ScrollView(do_scroll_x=False, size_hint=(1, None), size=(Window.width, Window.height*0.95))
		#gridlayout
		grd= GridLayout(size_hint=(1, None), cols=1, spacing=0)

		#gets the user requests
		#query
		sql = """SELECT * FROM Endereco_Entrega WHERE id_Consumidor=%s"""
		#data
		data = (self.control.model.user.id_c,)
		#get user endereco de entrega
		cursor = self.db.query(sql, data)
		#gets the results
		results = []
		#transform the fetch in dict
		for row_f in cursor.fetchall():
			#create a dict ex: {'col1': data1, 'col2': data2, 'col3': data3, ...}
			row = dict(zip(cursor.column_names, row_f))
			#and adds them to the results, so results items is a each row as a dict
			results.append(row)

		if len(results) == 0:
			l = Label(text='Nenhum Pedidio Feito', font_size='17sp', size_hint=(1, .1), pos_hint={'x': 0, 'y': .8})
			self.uplayout.add_widget(l)


		#binds the callbacks
		#self.ocbtn.bind(on_press=self.opn)


		#adds the screens widgets:
		#adds the grid layout to the scrollview
		scv.add_widget(grd)
		#adds the scrollview to the pedidos layout
		self.uplayout.add_widget(scv)
		#adds the filter button to the pedidos layout
		#self.uplayout.add_widget(self.ocbtn)
		#adds the uplayout to the m main layout
		self.add_widget(self.uplayout)
		#adds the filter layout to the main layout
		self.add_widget(self.flayout)


		#cerate canvas
		self.create_canvas()


	#method to ajust filter layout pos (to be called when the opening animation is done)
	def ajustf(self, animation, widget):
		#move the filter layout to be right on top of the pedidos layout
		self.flayout.pos_hint={'x': 0, 'y': 1}
		#set return button to move the filter layout to the right of the screen when pressed
		#unbinds the return button, so the close method to be called will be another sllightly different method
		self.control.interface.returnbtn.unbind(on_release=self.control.interface.screens['ConfigTab'].cls)
		#binds it to the another method
		self.control.interface.returnbtn.bind(on_release=self.closel)

	#method to close the main layout
	def closel(self, instance):
		#checks if the filter layout is open
		if self.ctrl:
			#if it is, closes it
			self.clse(self)

		#move return button out of the screen
		self.control.interface.returnbtn.pos_hint={'x': 1, 'y': 0.95}
		#move the config button to the screen again
		self.control.interface.configbtn.pos_hint={'x': 0, 'y': 0.95}
		#enables configs button
		self.control.interface.screens['ConfigTab'].payment.disabled = False
		self.control.interface.screens['ConfigTab'].pedidos.disabled = False
		self.control.interface.screens['ConfigTab'].dadosp.disabled = False
		self.control.interface.screens['ConfigTab'].fornecedores.disabled = False
		self.control.interface.screens['ConfigTab'].entrega.disbaled = False

		#creates the closing animation
		clse = Animation(x=0, y=Window.size[1]*(-1), t='out_quint', duration=0.4)
		#binds to clse to on complete remove Alscreen
		clse.bind(on_complete=self.control.interface.screens['ConfigTab'].remve)

		#return return button back to normal
		#binds the return button to the original close method
		self.control.interface.returnbtn.bind(on_release=self.control.interface.screens['ConfigTab'].clse)
		#unbinds it to the new method
		self.control.interface.returnbtn.unbind(on_release=self.closel)

		#start animation
		clse.start(self.control.interface.screens['ConfigTab'].scr)


	#method to close filter layout
	def clse(self, instance):
		#cretaes the animation
		clos = Animation(x=0, y=0, t='out_quint', duration=0.7)
		#binds the animation to whent its done it can be closed
		clos.bind(on_complete=self.opnbtn)
		#start animation
		clos.start(self)

	#method to open filter layout
	def opn(self, instance):
		#move the filter layout back to be right above the pedidos layout
		self.flayout.pos_hint={'x': 0, 'y': 1}
		#creates the animation
		y=(Window.size[1]*4/10)
		y*=(-1)
		opn = Animation(x=0, y=y, t='out_quint', duration=0.7)
		#binds the animation to whent its done it can be closed
		opn.bind(on_complete=self.clsbtn)
		#start animation
		opn.start(self)

	#method to set close button to close the filter layout cls
	def clsbtn(self, animation, widget):
		#move the filter layout to the right
		#self.flayout.pos_hint={'x': 1, 'y': 0}
		#set control variable to True, since the filter layout is open
		self.ctrl = True
		#unbinds to open filter layout
		self.ocbtn.unbind(on_press=self.opn)
		#binds to close filter layout
		self.ocbtn.bind(on_press=self.clse)

	#method to set open button to open the filter layout
	def opnbtn(self, animation, widget):
		#move filter layout back to the right
		self.flayout.pos_hint={'x': 1, 'y': 1}
		#set control variable to False, since the filter layout is close
		self.ctrl = False
		#unbinds to close filter layout
		self.ocbtn.unbind(on_press=self.clse)
		#binds to open filter layout
		self.ocbtn.bind(on_press=self.opn)

		
	#updates canvas
	def create_canvas(self):
		#adds Canvas instructions
		with self.canvas.before:
			#user pedidos background:
			#background color
			Color(.140, .140, .140, 1)
			#background itself
			self.rect = Rectangle(pos=self.uplayout.pos, size=self.uplayout.size)
			
			#filter background:
			#background color
			#Color(1, 1, 1, 1)
			#background itself
			#self.rect2 = Rectangle(pos=self.flayout.pos, size=self.flayout.size)

		#update the canvas instruction
		self.bind(pos=self.update_canvas, size=self.update_canvas)

	#atualiza a posicao e tamanho da instrucao do canvas
	def update_canvas(self, *args):
		#atualiza a pos
		self.rect.pos=self.uplayout.pos
		#self.rect2.pos=self.flayout.pos
		#atualiza o tamanho
		self.rect.size=self.uplayout.size
		#self.rect2.size=self.flayout.size
