'''
================================================================================================================================================================================================================================================
TELA DE ALTERACAO DE DADOS
================================================================================================================================================================================================================================================
'''

#json modules
import json
from json.decoder import JSONDecoder
from json.encoder import JSONEncoder

from kivy.animation import Animation, AnimationTransition
from kivy.config import Config
from kivy.core.window import Window, WindowBase
from kivy.clock import Clock
from kivy.graphics import *
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from functools import partial

class AlScreen(FloatLayout):
	def __init__(self, control, idt, jsn, *args, **kwargs):
		#define idt
		self.idt = idt

		#transforms JSON object to a python object
		jsn = JSONDecoder().decode(json.dumps(jsn))

		self.read(jsn, 0)
		#get the parent class functions
		super(AlScreen, self).__init__(*args, **kwargs)
		#pos and size of the screen
		y=Window.size[1]*(-1)
		self.pos=(0, y)
		self.size = Window.size
		#adds Canvas instructions
		with self.canvas:
			#background color
			Color(1, 1, 1, 1)
			#background itself
			self.rect = Rectangle(pos=self.pos, size=self.size)
		#update the canvas instruction
		self.bind(pos=self.update_canvas, size=self.update_canvas)
		
		#creates screen stuff saved in the json
		self.btn = Button(text='Hello World', pos_hint={'x': .25, 'y': .8}, size_hint=(.5 , .05), background_color=(1, 1, 1, 1))

		self.btn.bind(on_release=self.dl)
		
		#adds them
		self.add_widget(self.btn)

	def dl(self, instance):
		#habilita os botoes da aba de configuracao
		l = instance.parent.parent.children
		# configtab layout instance
		c = None
		#get the configtab layout instance
		for a in l:
			if a.idt == 'ConfigTab':
				#assigns it to variable c
				c = a

		for b in c.children:
			b.disabled = False
		
		#destroi a janela
		instance.parent.parent.remove_widget(instance.parent)

	#atualiza a posicao e tamanho da instrucao do canvas
	def update_canvas(self, *args):
		#atualiza a pos
		self.rect.pos=self.pos
		#atualiza o tamanho
		self.rect.size=self.size

	#reads the json
	def read(self, jsn, idt):
		#returns the input length of the data parameter received
		for i in jsn:
			#print i
			#print jsn[i]
			#i.encode('ascii','ignore')
			#unicodedata.normalize('NFKD', title).encode('ascii','ignore')
			b = str(type(jsn[i])) == "<type 'dict'>"
			#b = true if it is a dict, and false if it isnt
			#checks if the data is not a dict
			if not b:
				print idt*' '+str(i)+': '+str(jsn[i])+' ('+str(type(jsn[i]))+')'
			#if the data is a dict:
			else:
				print idt*' '+str(i)+':'
				nidt = idt + 4
				self.read(jsn[i], nidt)
		


'''
	def read(jsn):
		for i in jsn:
			i.encode('ascii','ignore')
			#unicodedata.normalize('NFKD', title).encode('ascii','ignore')
			b = str(type(jsn[i])) == "<type 'dict'>"
			#b = true if it is a dict, and false if it isnt
			#checks if the data isnt a dict
			if not b:
				#checks if the element is int
				try:
					#print type(jsn[i])
					d = int(jsn[i])
					print str(d) + str(type(d))
				except:
					#if it isnt, checks if it is supposed tu be a tuple
					if str(jsn[i])[0]=='(':
						#creates a tupple with every element
						tp = tuple(jsn[i])
						tp2 = []
						for e in tp:
							#for each element, checks it as int
							try:
								#change to int
								f = int(e)
								#change to float
								f = float(f)
								f = f/10
								#add the float to the array
								tp2.append(f)
							except:
								pass
						#generates the tuples based on the array
						tp3 = tuple(tp2)
						print str(tp3)+str(type(tp3))
					#it it isnt either, it is a string
					else:
						print str(jsn[i])+str(type(str(jsn[i])))
								
				#print '	'+str(i)+': '+str(jsn[i])+' ('+str(type(jsn[i]))+')'
				#if the data is a dict:
			else:
				print str(i)+':'
				self.read(jsn[i])


		
'''