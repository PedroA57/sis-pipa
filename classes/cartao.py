'''
================================================================================================================================================================================================================================================
TELA DE DADOS BANCARIOS DO CONSUMIDOR
================================================================================================================================================================================================================================================
'''


#import kivy modules
from kivy.animation import Animation, AnimationTransition
from kivy.config import Config
from kivy.core.window import Window, WindowBase
from kivy.clock import Clock
from kivy.graphics import *
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from functools import partial


class Cartao(FloatLayout):
	def __init__(self, control, db, idt, *args, **kwargs):
		super(Cartao, self).__init__(*args, **kwargs)
		#defines control class
		self.control = control
		#define idt
		self.idt = idt
		#assigns connection to the server to the db
		self.db = db
		#list com os cartoes registrados pegos no banco de dados
		self.cartoes = []
		#pos and size of the screen
		self.pos=(0, Window.size[1]*(-1))
		self.size=(Window.size[0], Window.size[1]*0.95)

		#creates screen stuff
		#text input layout | size hint is times 0.95 so it fits in the layout under the top bar
		self.tilayout = FloatLayout(size_hint=(1, (0.4*0.95)), pos_hint={'x': 0, 'y': (0.6*0.95)})
		#scroll view
		tiscv = ScrollView(do_scroll_x=False, size_hint=(1, None), size=(Window.width, Window.height*0.4*0.95), pos_hint={'x': 0, 'y': 0})
		#create grid layout
		grd = GridLayout(size_hint=(1, None), cols=1, spacing=Window.size[1]/35)
		grd.bind(minimum_height=grd.setter('height'))
		#crete the box layouts with the textinputs in it

		#image of text input
		image = 'Images/ti.png'

		#box0: (empty widget so the b1 widget dont start at the top)
		b0 = Widget(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/60)
		#adds it to the grid layout
		grd.add_widget(b0)

		#box1:
		b1 = FloatLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/20, orientation='vertical')
		#textinput1
		self.t1 = TextInput(text='', hint_text='Numero', multiline=False, size_hint=(.65, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})
		x = 1.00 - 0.09 - (500.0*Window.size[1]/(316.0*Window.size[0]*20))
		#Button to select bandeira
		self.t12 = Button(text='', size_hint=((128.0*Window.size[1]/(84.0*Window.size[0]*20)), 1), pos_hint={'x': x, 'y': 0}, background_normal='Images/Bandeira/visa-128px.png', background_down='Images/Bandeira/visa-128px.png', border=(0, 0, 0, 0))
		self.dropdown = DropDown()
		self.dropdown.bind(on_select=lambda instance, x: setattr(self.t12, 'background_normal', x))
		self.dropdown.bind(on_select=lambda instance, x: setattr(self.t12, 'background_down', x))
		self.t12.bind(on_release=self.dropdown.open)

		#provisorio
		btn1 = Button(text='', height=Window.size[1]/20, size_hint_y=None, background_normal='Images/Bandeira/visa-128px.png', background_down='Images/Bandeira/visa-128px.png', border=(0, 0, 0, 0))
		btn2 = Button(text='', height=Window.size[1]/20, size_hint_y=None, background_normal='Images/Bandeira/mastercard-128px.png', background_down='Images/Bandeira/mastercard-128px.png', border=(0, 0, 0, 0))
		btn3 = Button(text='', height=Window.size[1]/20, size_hint_y=None, background_normal='Images/Bandeira/bancodobrasil-128px.png', background_down='Images/Bandeira/bancodobrasil-128px.png', border=(0, 0, 0, 0))
		btn4 = Button(text='', height=Window.size[1]/20, size_hint_y=None, background_normal='Images/Bandeira/diners-128px.png', background_down='Images/Bandeira/diners-128px.png', border=(0, 0, 0, 0))
		btn1.bind(on_release = lambda btn: self.dropdown.select(btn.background_normal))
		btn2.bind(on_release = lambda btn: self.dropdown.select(btn.background_normal))
		btn3.bind(on_release = lambda btn: self.dropdown.select(btn.background_normal))
		btn4.bind(on_release = lambda btn: self.dropdown.select(btn.background_normal))
		self.dropdown.add_widget(btn1)
		self.dropdown.add_widget(btn2)
		self.dropdown.add_widget(btn3)
		self.dropdown.add_widget(btn4)

		#adds both textinput to the boxlayout
		b1.add_widget(self.t12)
		#adds textinput to the boxlayout
		b1.add_widget(self.t1)
		#adds boxlayout to the gridlayout
		grd.add_widget(b1)

		#box2
		b2 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/20, orientation='vertical')
		#textinput2
		self.t2 = TextInput(text='', hint_text='Nome', multiline=False, size_hint=(.82, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})

		#adds textinput to the boxlayout
		b2.add_widget(self.t2)
		#adds boxlayout to the gridlayout
		grd.add_widget(b2)

		#box3
		b3 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/20, orientation='vertical')
		#textinput4
		self.t3 = TextInput(text='', hint_text='Validade(MM/AA)', multiline=False, size_hint=(.5, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})
		
		b3.add_widget(self.t3)
		#adds the boxlayout to the gridlayout
		grd.add_widget(b3)

		#box4
		b4 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]*10/175, orientation='vertical')
		#button to submit
		self.btn4 = Button(text='Adicionar', size_hint=(.7, 1), pos_hint={'x': .15, 'y': 0}, background_color=(0.518, 0.643, 0.933, 1), background_normal='', background_down='')
		#bind the callback
		self.btn4.bind(on_release=self.submit)
		#adds the button to the boxlayout
		b4.add_widget(self.btn4)
		#adds the boxlayout to the gridlayout
		grd.add_widget(b4)

		#box7 (empty widget so the button in the box6 doesnt end at the bottom)
		b5 = Widget()
		#adds it to the gridlayout
		grd.add_widget(b5)

		#binds the callback
		#binds nexto method to the textinput to move the cursor when enter is pressed
		self.t1.bind(on_text_validate=partial(self.nexto, self.t2))
		self.t2.bind(on_text_validate=partial(self.nexto, self.t3))
		

		#user items layout | size hint is times 0.95 so it fits in the layout under the top bar
		self.uilayout = FloatLayout(size_hint=(1, (0.6*0.95)), pos_hint={'x': 0, 'y': 0})
		#scroll view
		uiscv = ScrollView(do_scroll_x=False, size_hint=(1, None), size=(Window.width, Window.height*0.6*0.95), pos_hint={'x': 0, 'y': 0})
		#create a querry to get the user pedidos
		sql = """SELECT * FROM Cartao WHERE id_Consumidor=%s"""
		#data
		data = (self.control.model.user.id_c,)
		#get user endereco de entrega
		cursor = self.db.query(sql, data)
		#gets the results
		results = []
		#transform the fetch in dict
		for row_f in cursor.fetchall():
			#create a dict ex: {'col1': data1, 'col2': data2, 'col3': data3, ...}
			row = dict(zip(cursor.column_names, row_f))
			#and adds them to the results, so results items is a each row as a dict
			results.append(row)

		#checks if there are any credit card registred
		if len(results)==0:
			l = Label(text='Nenhum Cartao de Credito Registrado', font_size='17sp', size_hint=(1, .1), pos_hint={'x': 0, 'y': .8})
			self.uilayout.add_widget(l)

		#adds them to the screen
		tiscv.add_widget(grd)
		self.tilayout.add_widget(tiscv)

		self.add_widget(self.tilayout)
		self.add_widget(self.uilayout)

		#create canvas
		self.create_canvas()


	#method to register the data in the database
	def submit(self, instance):
		return
		#check consistence
		if not self.check():
			#if there is any error, dont submit
			return
		else:
			#register in the database...
			#sql
			sql = """INSERT INTO Cartao (id_Consumidor, credito, numero, validade, nome_no_cartao, bandeira) VALUES (%s, %s, %s, %s, %s, %s)"""
			#get its credit card string
			creditcard = self.t12.background_normal[16:(len(self.t12.background_normal)-10)]
			#data
			data = (self.contro.model.id_c, 'credito', self.t1.text, self.t2.text, self.t3.text, creditcard)
			#execute the query
			self.db.data_query(sql, data)

			#notificate the user

			#calls configtabc method to close tab
			self.control.interface.screens['ConfigTab'].clse(instance)

	def check(self):
		#check if  the validade is alright
		if (len(self.t2.text) != 5) or (self.t2.text[2] != '/'):
			self.t2.text = ''
			self.t2.hint_text = 'Validade invalida'
			return False

		#check if the first an last characters are numbers
		try:
			m = int(self.t2.text[0, 2])
			m = int(self.t2.text[3, 5])

		except:
			self.t2.text = ''
			self.t2.hint_text = 'Validade invalida'
			return False

	#method to move the cursor to the next textinput
	def nexto(self, nexto, instance):
		#move the cursor to the next texinput
		nexto.focus = True


	#updates canvas
	def create_canvas(self):
		#adds Canvas instructions
		with self.canvas.before:
			#textinput background:
			#background color
			Color(1, 1, 1, 1)
			#background itself
			self.rect = Rectangle(pos=self.tilayout.pos, size=self.tilayout.pos)
			
			#user items background:
			#background color
			Color(.140, .140, .140, 1)
			#background itself
			self.rect2 = Rectangle(pos=self.uilayout.pos, size=self.uilayout.pos)

		#update the canvas instruction
		self.bind(pos=self.update_canvas, size=self.update_canvas)


	#atualiza a posicao e tamanho da instrucao do canvas
	def update_canvas(self, *args):
		#atualiza a pos
		self.rect.pos=self.tilayout.pos
		self.rect2.pos=self.uilayout.pos
		#atualiza o tamanho
		self.rect.size=self.tilayout.size
		self.rect2.size=self.uilayout.size



	def dl(self, instance):
		#habilita os botoes da aba de configuracao
		l = instance.parent.parent.children
		# configtab layout instance
		c = None
		#get the configtab layout instance
		for a in l:
			if a.idt == 'ConfigTab':
				#assigns it to variable c (c=configtab.layout)
				c = a

		#runs for every button in c.scv.grd.children(configtab.layout.scv.grd.children=childs of the gridlayout within the scrollview within the configtab.layout)
		for b in c.scv.grd.children:
			#enables it beacause they were disbaled when the ascreen layout was open
			b.disabled = False
		
		#destroi a janela
		instance.parent.parent.remove_widget(instance.parent)
		
