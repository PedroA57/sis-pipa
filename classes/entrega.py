'''
================================================================================================================================================================================================================================================
TELA DE ENDERECO DE ENTREGA DO CONSUMIDOR
================================================================================================================================================================================================================================================
'''

from kivy.animation import Animation, AnimationTransition
from kivy.config import Config
from kivy.core.window import Window, WindowBase
from kivy.clock import Clock
from kivy.graphics import *
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from functools import partial

class Entrega(FloatLayout):
	def __init__(self, control, db, idt, *args, **kwargs):
		#defines control
		self.control = control
		#define idt
		self.idt = idt
		#assigns connection to the server to the db
		self.db = db
		#get the parent class functions
		super(Entrega, self).__init__(*args, **kwargs)
		#list com os endereco de entrega registrados pegos no banco de dados
		self.enderecos = []
		#pos and size of the screen
		self.pos=(0, Window.size[1]*(-1))
		self.size=(Window.size[0], Window.size[1]*0.95)

		#creates screen stuff
		#text input layout | size hint is times 0.95 so it fits in the layout under the top bar
		self.tilayout = FloatLayout(size_hint=(1, (0.5*0.95)), pos_hint={'x': 0, 'y': (0.5*0.95)})
		#scroll view
		tiscv = ScrollView(do_scroll_x=False, size_hint=(1, None), size=(Window.width, Window.height*0.5*0.95), pos_hint={'x': 0, 'y': 0})
		#create grid layout
		grd = GridLayout(size_hint=(1, None), cols=1, spacing=Window.size[1]/50)
		grd.bind(minimum_height=grd.setter('height'))
		#crete the box layouts with the textinputs in it

		#image of text input
		image = 'Images/ti.png'

		#box0: (empty widget so the b1 widget dont start at the top)
		b0 = Widget()
		#adds it to the grid layout
		grd.add_widget(b0)

		#box1:
		b1 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/20, orientation='vertical')
		#textinput1
		self.t1 = TextInput(text='', hint_text='Logradouro', multiline=False, size_hint=(.82, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})

		#adds textinput to the boxlayout
		b1.add_widget(self.t1)
		#adds boxlayout to the gridlayout
		grd.add_widget(b1)

		#box2
		b2 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/20, orientation='vertical')
		#textinput2
		self.t2 = TextInput(text='', hint_text='Cidade', multiline=False, size_hint=(.82, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})

		#adds textinput to the boxlayout
		b2.add_widget(self.t2)
		#adds boxlayout to the gridlayout
		grd.add_widget(b2)

		#box3
		b3 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/20, orientation='vertical')
		#textinput3
		self.t3 = TextInput(text='', hint_text='Bairro', multiline=False, size_hint=(.82, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})

		#adds textinput to the boxlayout
		b3.add_widget(self.t3)
		#adds boxlayout to the gridlayout
		grd.add_widget(b3)

		#box4
		b4 = FloatLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]/20, orientation='vertical')
		#textinput4
		self.t4 = TextInput(text='', hint_text='Estado', multiline=False, size_hint=(.478, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})
		#textinput4 2
		self.t42 = TextInput(text='', hint_text='CEP', multiline=False, size_hint=(.292, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .618, 'y': 0})

		#adds both textinput to the boxlayout
		b4.add_widget(self.t42)
		b4.add_widget(self.t4)
		#adds the boxlayout to the gridlayout
		grd.add_widget(b4)

		#box5
		b5 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]*10/74, orientation='vertical')
		#textinput5
		self.t5 = TextInput(text='', hint_text='Referencia', multiline=True, size_hint=(.82, 1), font_size=(15*(float(Window.height)/500.0)), pos_hint={'x': .09, 'y': 0})

		#adds the textinput to the boxlayout
		b5.add_widget(self.t5)
		#adds the boxlayout to gridlayout
		grd.add_widget(b5)

		#box6
		b6 = BoxLayout(size_hint=(None, None), width=Window.size[0], height=Window.size[1]*10/175, orientation='vertical')
		#button to submit
		self.btn6 = Button(text='Adicionar', size_hint=(.7, 1), pos_hint={'x': .15, 'y': 0}, background_color=(0.518, 0.643, 0.933, 1), background_normal='', background_down='')
		#bind the callback
		self.btn6.bind(on_release=self.submit)
		#adds the button to the boxlayout
		b6.add_widget(self.btn6)
		#adds the boxlayout to the gridlayout
		grd.add_widget(b6)

		#box7 (empty widget so the button in the box6 doesnt end at the bottom)
		b7 = Widget()
		#adds it to the gridlayout
		grd.add_widget(b7)

		#binds the callback
		#binds nexto method to the textinput to move the cursor when enter is pressed
		self.t1.bind(on_text_validate=partial(self.nexto, self.t2))
		self.t2.bind(on_text_validate=partial(self.nexto, self.t3))
		self.t3.bind(on_text_validate=partial(self.nexto, self.t4))
		self.t4.bind(on_text_validate=partial(self.nexto, self.t42))
		self.t42.bind(on_text_validate=partial(self.nexto, self.t5))


		#user items layout | size hint is times 0.95 so it fits in the layout under the top bar
		self.uilayout = FloatLayout(size_hint=(1, (0.5*0.95)), pos_hint={'x': 0, 'y': 0})
		#scroll view
		uiscv = ScrollView(do_scroll_x=False, size_hint=(1, None), size=(Window.width, Window.height*0.6*0.95), pos_hint={'x': 0, 'y': 0})
		#create a querry to get the user pedidos
		sql = """SELECT * FROM Endereco_Entrega WHERE id_Consumidor=%s"""
		#data
		data = (self.control.model.user.id_c,)
		#get user endereco de entrega
		cursor = self.db.query(sql, data)
		#gets the results
		results = []
		#transform the fetch in dict
		for row_f in cursor.fetchall():
			#create a dict ex: {'col1': data1, 'col2': data2, 'col3': data3, ...}
			row = dict(zip(cursor.column_names, row_f))
			#and adds them to the results, so results items is a each row as a dict
			results.append(row)

		self.enderecos = results
		#checks if there are any credit card registred
		if len(self.enderecos)==0:
			l = Label(text='Nenhum Enedereco de Entrega Registrado', font_size='17sp', size_hint=(1, .1), pos_hint={'x': 0, 'y': .8})
			self.uilayout.add_widget(l)


		#adds them to the screen
		tiscv.add_widget(grd)
		self.tilayout.add_widget(tiscv)

		self.add_widget(self.tilayout)
		self.add_widget(self.uilayout)

		#create canvas
		self.create_canvas()


	#method to register the data in the database
	def submit(self, instance):
		#register in the database...
		#sql
		sql="""INSERT INTO Endereco_Entrega (id_Consumidor, logadrouro, bairro, cidade, estado, CEP, referencia) VALUES (%s, %s, %s, %s)"""
		#data
		data = (self.control.model.user.id_c, self.t1.text, self.t3.text, self.t2.text, self.t4.text, self.t42.text, self.t5.text)
		#execute the sql
		self.db.data_query(sql, data)

		#notificate the user

		#calls configtabc method to close tab
		self.control.interface.screens['ConfigTab'].clse(instance)

	#method to move the cursor to the next textinput
	def nexto(self, nexto, instance):
		#move the cursor to the next texinput
		nexto.focus = True


	#updates canvas
	def create_canvas(self):
		#adds Canvas instructions
		with self.canvas.before:
			#textinput background:
			#background color
			Color(1, 1, 1, 1)
			#background itself
			self.rect = Rectangle(pos=self.tilayout.pos, size=self.tilayout.pos)
			
			#user items background:
			#background color
			Color(.140, .140, .140, 1)
			#background itself
			self.rect2 = Rectangle(pos=self.uilayout.pos, size=self.uilayout.pos)

		#update the canvas instruction
		self.bind(pos=self.update_canvas, size=self.update_canvas)


	#atualiza a posicao e tamanho da instrucao do canvas
	def update_canvas(self, *args):
		#atualiza a pos
		self.rect.pos=self.tilayout.pos
		self.rect2.pos=self.uilayout.pos
		#atualiza o tamanho
		self.rect.size=self.tilayout.size
		self.rect2.size=self.uilayout.size



	def dl(self, instance):
		#habilita os botoes da aba de configuracao
		l = instance.parent.parent.children
		# configtab layout instance
		c = None
		#get the configtab layout instance
		for a in l:
			if a.idt == 'ConfigTab':
				#assigns it to variable c (c=configtab.layout)
				c = a

		#runs for every button in c.scv.grd.children(configtab.layout.scv.grd.children=childs of the gridlayout within the scrollview within the configtab.layout)
		for button in c.scv.grd.children:
			#enables it beacause they were disbaled when the ascreen layout was open
			button.disabled = False
		
		#destroi a janela
		instance.parent.parent.remove_widget(instance.parent)


	#reads the json
	def read(self, jsn, idt):
		#returns the input length of the data parameter received
		for i in jsn:
			#print i
			#print jsn[i]
			#i.encode('ascii','ignore')
			#unicodedata.normalize('NFKD', title).encode('ascii','ignore')
			b = str(type(jsn[i])) == "<type 'dict'>"
			#b = true if it is a dict, and false if it isnt
			#checks if the data is not a dict
			if not b:
				print idt*' '+str(i)+': '+str(jsn[i])+' ('+str(type(jsn[i]))+')'
			#if the data is a dict:
			else:
				print idt*' '+str(i)+':'
				nidt = idt + 4
				self.read(jsn[i], nidt)
		


'''
	def read(jsn):
		for i in jsn:
			i.encode('ascii','ignore')
			#unicodedata.normalize('NFKD', title).encode('ascii','ignore')
			b = str(type(jsn[i])) == "<type 'dict'>"
			#b = true if it is a dict, and false if it isnt
			#checks if the data isnt a dict
			if not b:
				#checks if the element is int
				try:
					#print type(jsn[i])
					d = int(jsn[i])
					print str(d) + str(type(d))
				except:
					#if it isnt, checks if it is supposed tu be a tuple
					if str(jsn[i])[0]=='(':
						#creates a tupple with every element
						tp = tuple(jsn[i])
						tp2 = []
						for e in tp:
							#for each element, checks it as int
							try:
								#change to int
								f = int(e)
								#change to float
								f = float(f)
								f = f/10
								#add the float to the array
								tp2.append(f)
							except:
								pass
						#generates the tuples based on the array
						tp3 = tuple(tp2)
						print str(tp3)+str(type(tp3))
					#it it isnt either, it is a string
					else:
						print str(jsn[i])+str(type(str(jsn[i])))
								
				#print '	'+str(i)+': '+str(jsn[i])+' ('+str(type(jsn[i]))+')'
				#if the data is a dict:
			else:
				print str(i)+':'
				self.read(jsn[i])


		
'''