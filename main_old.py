#mysql modules
'''
import mysql.connector
from mysql.connector import *
'''
#name: Tre4gh-L0l

#android modules
#from jnius import autoclass

#kivy modules
import kivy
kivy.require('1.1.1')

#json modules
import json

#to manipulate the data in unicode from json
import unicodedata


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty
from kivy.vector import Vector
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, SlideTransition, WipeTransition
from kivy.uix.accordion import Accordion, AccordionItem
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window, WindowBase
from kivy.config import Config
from kivy.animation import Animation, AnimationTransition
from functools import partial
from kivy.graphics import *

#import classes
from classes.initial import Initial
from classes.initialogged_user import Initialogged_user
from classes.initialogged_forn import Initialogged_forn
from classes.userdata import Userdata



########################################################################################################################################
######################## CAMADA DE APRESENTACAO ########################################################################################
########################################################################################################################################

# (Initial, Initialoged, Configtab) -> SManager -> Interface -> App

class ConfigTab(Widget):
    def __init__(self, *args, **kwargs):
        #gets the screen class stuff
        super(ConfigTab, self).__init__(*args, **kwargs)
    
        #adds things to the interface
        #layout of the screen (y: .9727)
        x = Window.size[0]*(-0.8)
        self.layout=FloatLayout(size_hint=(.8, 1), pos_hint={'x':-0.8, 'y':0})
        #self.layout.canvas.add(Color(1, 0, 0, 1))
        #self.layout.canvas.add(Rectangle(pos=self.layout.pos, size=self.layout.size))
        #xy of the window
        x = Window.size[0]
        y = Window.size[1]
        #user interface
        self.user_bar=Widget(size_hint=(1, .225), pos_hint={'x': 0, 'y': .775})

        #adds the canvas instructions
        with self.user_bar.canvas:
        	#lightsky color
        	Color(.529, .807, .98, 1)
        	self.rect = Rectangle(pos=self.user_bar.pos, size= self.user_bar.size)
        	Color(1, 1, 1, 1)
        	self.ell = Ellipse(source='/home/void/Desktop/Scripts/sis-pipa/Images/profile-42914_640.png', pos=self.user_bar.pos, size= self.user_bar.size)
        
        #updates the canvas instructions pos and size
        self.user_bar.bind(pos=control.canvas_update, size=control.canvas_update)

        #settings tabs:
        self.userdata = Button(text='dados pessoais', size_hint=(1, .1), background_color=(1, 1, 1, 1), pos_hint={'x':0, 'y': .675})

        #binds the callback
        self.userdata.bind(on_release=self.openuserdata)
        
        self.layout.add_widget(self.user_bar)
        self.layout.add_widget(self.userdata)
        #adds the layout to te screen
        #self.add_widget(self.layout)

    #opens the dados pessoais tab
    def openuserdata(self, instance):
    	#closes the config tab
        control.config(.1, None)
        #tela de alteracao de dados pessoais
        self.userdatascreen = Userdata(control)
        #abre ele
        control.openalter(True,)


class SManager(ScreenManager):
    def __init__(self, *args, **kwargs):
        #gets the screen class stuff
        super(SManager, self).__init__(*args, **kwargs)

        #generates the screen objects
        self.initial = Initial(name='initial', control=control)
        self.initialogged_user = Initialogged_user(name='initialogged_user', control=control)
        self.initialogged_forn = Initialogged_forn(name='initialogged_forn', control=control)
        #self.configtab = ConfigTab(name='configtab')
    
        #sets its own variables
        self.size_hint=(1, .95)
        self.pos_hint={'x':0, 'y': 0}

        #adds the screen
        self.add_widget(self.initial)
        self.add_widget(self.initialogged_user)
        self.add_widget(self.initialogged_forn)
        #self.add_widget(self.configtab)
        #sets the current screen
        self.current='initial'


interface=0
class Interface(Widget):
    def __init__(self, *args, **kwargs):
        super(Interface, self).__init__(*args, **kwargs)
        #variables
        self.actual=''
        self.cbar = False
        
        self.layout=FloatLayout()
        #screens
        self.SM = SManager()
        #Config tab
        self.config = ConfigTab()
        #top bar
        self.bar = Button(text='', disabled = True, size_hint=(1, .1), pos_hint={'x':0, 'y': .95}, background_disabled_normal='', background_color= (.5, .5, .5, 1))
        #config Button / pos_hint={'x': .01, 'y': .98}
        sizey = Window.size[1]/20
        sizex = sizey*12/10
        #1: pc, 2: mobile
        self.image='/home/void/Desktop/Scripts/sis-pipa/Images/bars_config.png'
        #self.image='bars_config.png'
        self.configbtn = Button(text='', size_hint=(None, None), size=(sizex, sizey), pos_hint={'x': 0, 'y': .95}, background_normal = self.image, background_down = self.image, border = (0, 0, 0, 0))

        #binds the callbacks
        self.configbtn.bind(on_release=partial(control.config, 0.85))
        
        #adds them to the layout
        self.layout.add_widget(self.SM)
        self.layout.add_widget(self.config.layout)
        self.layout.add_widget(self.bar)
        self.layout.add_widget(self.configbtn)


##########################################################################################################################################
######################### Camada de Dados ################################################################################################
##########################################################################################################################################

#apagar camada de dados, colcoar funcoes na camada de controle
data=0
class Model():
	def __init__(self):
		#connect to te server...
		self.connection = False
		self.user = None

	def login(self):
		#for now...
		#validar entrada pelo json
		if interface.SM.initial.usernameinput.text == 'Consumidor' or interface.SM.initial.usernameinput.text == 'c':
			return 'Consumidor'
		elif interface.SM.initial.usernameinput.text == 'Fornecedor':
			return 'Fornecedor'
		#try the login
		#...
		#if its succeds
		#return the type of the login
		#if it doenst
		return False


########################################################################################################################################
######################### Camada de Controle ###########################################################################################
########################################################################################################################################


control=0
class Controller():
	def __init__(self):
		with open("/home/void/Desktop/Scripts/sis-pipa/data/json_data") as json_file:
			self.json_data = json.load(json_file)
		self.logged = False
		self.bar = False
		self.actual = 0

    #does the login transition
	def login(self, instance):
		if data.login() == 'Consumidor':
			#changes the variables about login
			data.user = 'Consumidor'
			self.logged=True
			#change the screen
			interface.SM.transition.direction='left'
			interface.SM.current='initialogged_user'
			#reset text input
			interface.SM.initial.reset('')
			#adds the consumidor config tabs
			self.add_tabs()
			
		elif data.login() == 'Fornecedor':
			#change the variables about login
			data.user = 'Fornecedor'
			self.logged=True
			#change the screen
			interface.SM.transition.direction='left'
			interface.SM.current='initialogged_forn'
			#reset text input
			interface.SM.initial.reset('')

		else:
			interface.SM.initial.reset('Usuario ou Senha Incorretas')
			interface.SM.initial.usernameinput.hint_text_color=(1, 0, 0, 1)
			

			interface.SM.initial.passwordinput.hint_text=''

    #informs the user the login failed
	def loginerror(self, instance):
		if interface.SM.initial.usernameinput.hint_text=='Usuario ou Senha Incorretas':
			interface.SM.initial.usernameinput.hint_text=''
			interface.SM.initial.usernameinput.hint_text_color=(.5, .5, .5, 1)
		elif interface.SM.initial.usernameinput.hint_text=='':
			interface.SM.initial.usernameinput.hint_text='Usuario'
			interface.SM.initial.usernameinput.hint_text_color=(.5, .5, .5, 1)
		if interface.SM.initial.passwordinput.hint_text=='':
			interface.SM.initial.passwordinput.hint_text='Senha'
			interface.SM.initial.passwordinput.hint_text_color=(.5, .5, .5, 1)


    #does the logout transition
	def logout(self, instance):
		self.logged=False
		interface.SM.initial.reset('Usuario')
		interface.SM.initial.usernameinput.text=''
		interface.SM.transition.direction='right'
		interface.SM.current='initial'

    #opens and closes the config tab
	def config(self, t, instance):
		#checks if the user is logged already
		if not self.logged:
			return

		#if the tab is closed, it open it
		if not self.bar:
			x = Window.size[0]*8/10
			opn = Animation(x=x, y=0, t='out_cubic', duration=t)
			opn.start(interface.layout)
			self.bar = True

		#if the tab is opened, close it
		elif self.bar:
			x = Window.size[0]*(-8/10)
			opn = Animation(x=0, y=0, t='out_cubic', duration=t)
			opn.start(interface.layout)
			#sm.current=self.actual
			self.bar = False

	def add_tabs(self):
		y = Window.size[1]
		r = interface.config
		r.fornecedores = Button(text='Fornecedores', size_hint=(1, .1), background_color=(1, 1, 1, 1), pos_hint={'x': 0, 'y': .575})
		r.layout.add_widget(r.fornecedores)
		r.dadosbancarios = Button(text='Pagamento', size_hint=(1, .1), background_color=(1, 1, 1, 1), pos_hint={'x': 0, 'y': .475})
		r.layout.add_widget(r.dadosbancarios)


    #returns the data length and adds them to the dict
	def get_data_length(self, data, a):
		#returns the input length of the data parameter received
		length = 0
		for i in data:
			i.encode('ascii','ignore')
			#unicodedata.normalize('NFKD', title).encode('ascii','ignore')
			b = str(type(data[i])) == "<type 'dict'>"
			#b = true if it is a dict, and false if it isnt

			#checks if the data is a dict and this was called in length mode
			if not b:
				a.append(i)
				length+=1
			#if the data is a dict and it was called in length mode:
			else:
				#it calls itself to get length of the dict in the data
				length = length + self.get_data_length(data[i], a)

		return length


	def openalter(self, b, w):
		if b:


	#updates the canvas pos and size
	def canvas_update(self, *args):
		#updates background pos and size
		interface.config.rect.pos=interface.config.user_bar.pos
		interface.config.rect.size=interface.config.user_bar.size
		#sets variables with pos x y, and size x y of the user image
		size_x = interface.config.user_bar.size[0]
		size_y = interface.config.user_bar.size[1]*8/10
		pos_x = interface.config.user_bar.pos[0] + Window.size[0]/20
		pos_y = interface.config.user_bar.pos[1] + interface.config.user_bar.size[1]/10
		interface.config.ell.pos=(pos_x, pos_y)
		interface.config.ell.size=(size_y, size_y)




########################################################################################################################################
########################################################################################################################################
########################################################################################################################################


class WTApp(App):
    def build(self):
    	global control, interface, data
    	data = Model()
    	control = Controller()
    	interface = Interface()
    	return interface.layout

    def on_pause(self):
        return True

    def on_start(self):
        pass

#runs the app
if __name__ == '__main__':
    WTApp().run()
